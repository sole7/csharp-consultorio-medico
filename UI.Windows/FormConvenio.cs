﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormConvenio : Form
    {
        Convenio objConv = new Convenio();
        ConvenioBll objConvBll = new ConvenioBll();

        public string filtro;

        public FormConvenio()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {                
                objConv.Cnpj = maskeCNPJ.Text;
                objConv.Nome = txtNome.Text;
                objConv.Contato = txtContato.Text;
                objConv.Telefone = maskTxtTelFixo.Text;
                          

                objConvBll.IncluirConvenio(objConv);

                objConv.IdConvenio = Convert.ToInt32(lblidConvenio.Text);

                if (objConvBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                label4.Text = objConvBll.Mensagem;

                AtualizarGrid();

            }
            catch
            {
                label4.Text = objConvBll.Mensagem;
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                objConv.IdConvenio = Convert.ToInt32(lblidConvenio.Text);
                objConv.Cnpj = maskeCNPJ.Text ;
                objConv.Nome = txtNome.Text;
                objConv.Contato = txtContato.Text;
                objConv.Telefone =  maskTxtTelFixo.Text;

                objConvBll.AlterarConvenio(objConv);

                if (objConvBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                label4.Text = objConvBll.Mensagem;

                AtualizarGrid();

            }
            catch
            {
                label4.Text = "Não foi possível a conexão com o banco";
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaControles(this);
        }

        private void dgvMedico_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            lblidConvenio.Text = dgvConvenio[0,dgvConvenio.CurrentRow.Index].Value.ToString();
            maskeCNPJ.Text = dgvConvenio[1, dgvConvenio.CurrentRow.Index].Value.ToString();
            txtNome.Text = dgvConvenio[2, dgvConvenio.CurrentRow.Index].Value.ToString();           
            txtContato.Text = dgvConvenio[3, dgvConvenio.CurrentRow.Index].Value.ToString();
            maskTxtTelFixo.Text = dgvConvenio[4, dgvConvenio.CurrentRow.Index].Value.ToString();
        
        }

        private void FormatarGrid()
        {
            try
            {
                dgvConvenio.Columns[0].HeaderText= " Número";
                dgvConvenio.Columns[0].Width = 100 ;


                dgvConvenio.Columns[1].HeaderText = " CNPJ";
                dgvConvenio.Columns[1].Width = 150;

                dgvConvenio.Columns[2].HeaderText= "Nome ";
                dgvConvenio.Columns[2].Width = 210 ;
                            
                dgvConvenio.Columns[3].HeaderText=" Contato";
                dgvConvenio.Columns[3].Width = 180 ;

                
                dgvConvenio.Columns[4].HeaderText="Telefone";
                dgvConvenio.Columns[4].Width = 130 ;
            
              
            }
            catch
            {
                label4.Text = "Não foi possível a conexão com o banco! ";
            }

        }

        private void AtualizarGrid()
        {
            filtro = txtPesquisar.Text;

            try
            {
                dgvConvenio.DataSource = objConvBll.Listagem(filtro);
                
            }
            catch 
            {
                label4.Text = "Não foi possível a conexão com o banco! ";
            }
            finally
            {
                FormatarGrid();
            }

        }
        

        private void FormConvenio_Load(object sender, EventArgs e)
        {
            try
            {
                if (Globais.ChamadaConsulta == true)
                {
                    btnAlterar.Visible = false;
                    btnIncluir.Visible = false;
                }
                AtualizarGrid();

                // Estabelecendo e exibindo a data do sistema
                string strData, strDia, strMes, strAno;
                strDia = DateTime.Now.Day.ToString("00");
                strMes = DateTime.Now.Month.ToString("00");
                strAno = DateTime.Now.Year.ToString("0000");
                strData = strDia + "/" + strMes + "/" + strAno;
                lbldata.Text = strData;

                timer1.Enabled = true;

                timer1.Interval = 1000;
            }
            catch
            {
                label4.Text = "Não foi possível a conexão com o banco !";
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRelogio, strHora, strMinuto, strSegundo;
            strHora = DateTime.Now.Hour.ToString("00");
            strMinuto = DateTime.Now.Minute.ToString("00");
            strSegundo = DateTime.Now.Second.ToString("00");
            strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
            lblhora.Text = strRelogio;
        }


        private void limpaControles(Control controle)
        {
            lblidConvenio.Text = "";
            label4.Text = "";

            foreach (Control ctrl in controle.Controls)
            {

                if (ctrl is TextBox)
                {                           //vazio
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }

                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
            }

        }

        private void btnFiltrar_Click_1(object sender, EventArgs e)
        {            
            try
            {
                AtualizarGrid();
                limpaControles(this);
            }
            catch
            {
                label4.Text = "Pesquisa não encontrada!";
            }
        }

       

      
       

        
    }
}
