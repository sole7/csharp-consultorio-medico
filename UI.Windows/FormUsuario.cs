﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.Modelo;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormUsuario : Form
    {
        Usuario objUsuario = new Usuario();
        UsuarioBll objUsuarioBll = new UsuarioBll();
        public string filtro ;
        

        public FormUsuario()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                objUsuario.Nome = txtLogin.Text;
                objUsuario.Senha = txtSenha.Text;
                objUsuario.Tipo = cboTipo.Text;

                objUsuarioBll.IncluirUsuario(objUsuario);

                lblIdUsuario.Text = objUsuario.Id.ToString();
                       

                if(objUsuarioBll.Mensagem.Contains("sucesso"))
                    label2.ForeColor = Color.Blue;
                else
                    label2.ForeColor = Color.Red;

                label2.Text = objUsuarioBll.Mensagem;
                
                AtualzarGrid();
               
            }
            catch
            {
                label2.Text = "Desculpe, falha na conexão com o banco!";
            }
           
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                objUsuario.Id = Convert.ToInt32(lblIdUsuario.Text);
                objUsuario.Nome = txtLogin.Text;
                objUsuario.Senha = txtSenha.Text;
                objUsuario.Tipo = cboTipo.Text;

                objUsuarioBll.AlterarUsuario(objUsuario);

                if (objUsuarioBll.Mensagem.Contains("sucesso"))
                    label2.ForeColor = Color.Blue;
                else
                    label2.ForeColor = Color.Red;

                label2.Text = objUsuarioBll.Mensagem;
                AtualzarGrid();

            }
            catch
            {
                label2.Text = "Desculpe, falha na conexão com o banco!";
            }


        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {                        
                  
                objUsuario.Id = Convert.ToInt32(lblIdUsuario.Text);
                objUsuarioBll.ExcluirUsuario(objUsuario);

                if (objUsuarioBll.Mensagem.Contains("sucesso"))
                    label2.ForeColor = Color.Blue;
                else
                    label2.ForeColor = Color.Red;

                label2.Text = objUsuarioBll.Mensagem;
                AtualzarGrid();
                                
            }
            catch
            {
                label2.Text = "Desculpe, falha na conexão com o banco!";
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaControles(this);
        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            lblIdUsuario.Text = dgvUsuarios[0, dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtLogin.Text = dgvUsuarios[1,dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtSenha.Text = dgvUsuarios[2,dgvUsuarios.CurrentRow.Index].Value.ToString();
            cboTipo.Text = dgvUsuarios[3,dgvUsuarios.CurrentRow.Index].Value.ToString();

        }

        public void FormatarGrid()
        {
            dgvUsuarios.Columns[0].HeaderText = "Id Usuário";
            dgvUsuarios.Columns[0].Width = 60 ;
            dgvUsuarios.Columns[1].HeaderText = "Login";
            dgvUsuarios.Columns[0].Width = 160;
            dgvUsuarios.Columns[2].HeaderText = "Senha";
            dgvUsuarios.Columns[0].Width = 80;
            dgvUsuarios.Columns[3].HeaderText = "Tipo";
            dgvUsuarios.Columns[0].Width = 120;
            

        }

        public void AtualzarGrid()
        {
            filtro = txtPesquisar.Text;
            try
            {
                dgvUsuarios.DataSource = objUsuarioBll.Listagem(filtro);
            }
            catch
            {
                label2.Text = "Desculpe, falha na conexão com o banco!";
            }
            finally
            {
                FormatarGrid();
                
            }

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {           
            try
            {               
                AtualzarGrid();
                limpaControles(this);
            
            }
            catch
            {
                label2.Text = "Pesquisa não encontrada!";
            }

        }

        private void FormUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                AtualzarGrid();
            }
            catch
            {
                label2.Text = "Desculpe, falha na conexão com o banco!";
           

            }
        }

        private void limpaControles(Control controle)
        {
            lblIdUsuario.Text = "";
            label2.Text = "";
            cboTipo.Text = "";
            foreach (Control ctrl in controle.Controls)
            {
                if (ctrl is TextBox)
                {                          
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }

                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
            }

        }

        

        
    

       

        }


  }

