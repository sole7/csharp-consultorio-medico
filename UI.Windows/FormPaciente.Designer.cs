﻿namespace Clinica
{
    partial class FormPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label13 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtPesquisar = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProntuario = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxSexo = new System.Windows.Forms.ComboBox();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dTPDataNasc = new System.Windows.Forms.DateTimePicker();
            this.maskeCPF = new System.Windows.Forms.MaskedTextBox();
            this.maskedRG = new System.Windows.Forms.MaskedTextBox();
            this.maskTxtCelular = new System.Windows.Forms.MaskedTextBox();
            this.maskTxtTelFixo = new System.Windows.Forms.MaskedTextBox();
            this.btnIncluir = new System.Windows.Forms.ToolStripButton();
            this.btnAlterar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLimpar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.txtConvenio = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtConsultarIdConvenio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbldata = new System.Windows.Forms.Label();
            this.lblhora = new System.Windows.Forms.Label();
            this.ttpPaciente = new System.Windows.Forms.ToolTip(this.components);
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(586, 138);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 20);
            this.label13.TabIndex = 69;
            this.label13.Text = "Email ";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(468, 137);
            this.txtComplemento.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(107, 21);
            this.txtComplemento.TabIndex = 81;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(94, 135);
            this.txtEndereco.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(245, 21);
            this.txtEndereco.TabIndex = 80;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(352, 137);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 20);
            this.label12.TabIndex = 68;
            this.label12.Text = "Complemento ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 135);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 20);
            this.label11.TabIndex = 67;
            this.label11.Text = "Endereço ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 86);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 66;
            this.label10.Text = "Celular *";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(222, 84);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 20);
            this.label9.TabIndex = 65;
            this.label9.Text = "Tel Fixo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(397, 35);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 20);
            this.label8.TabIndex = 63;
            this.label8.Text = "RG ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(222, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 59;
            this.label2.Text = "CPF";
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.Location = new System.Drawing.Point(83, 33);
            this.txtNome.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(124, 21);
            this.txtNome.TabIndex = 52;
            this.ttpPaciente.SetToolTip(this.txtNome, "campo obrigatório");
            // 
            // txtPesquisar
            // 
            this.txtPesquisar.Location = new System.Drawing.Point(55, 14);
            this.txtPesquisar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPesquisar.Name = "txtPesquisar";
            this.txtPesquisar.Size = new System.Drawing.Size(184, 21);
            this.txtPesquisar.TabIndex = 83;
            this.ttpPaciente.SetToolTip(this.txtPesquisar, "digite nome ou prontuário do paciente!");
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvUsuarios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvUsuarios.Location = new System.Drawing.Point(28, 331);
            this.dgvUsuarios.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.Size = new System.Drawing.Size(876, 158);
            this.dgvUsuarios.TabIndex = 54;
            this.dgvUsuarios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsuarios_CellContentClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 49;
            this.label6.Text = "Filtrar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 48;
            this.label3.Text = "Nome*  ";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(642, 138);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(146, 21);
            this.txtEmail.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Prontuário";
            // 
            // lblProntuario
            // 
            this.lblProntuario.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblProntuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProntuario.Location = new System.Drawing.Point(111, 59);
            this.lblProntuario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProntuario.Name = "lblProntuario";
            this.lblProntuario.Size = new System.Drawing.Size(70, 20);
            this.lblProntuario.TabIndex = 47;
            this.ttpPaciente.SetToolTip(this.lblProntuario, "campo preenchido automático");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(541, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 70;
            this.label4.Text = "Sexo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(658, 39);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 20);
            this.label5.TabIndex = 73;
            this.label5.Text = "Data Nasc";
            // 
            // cbxSexo
            // 
            this.cbxSexo.FormattingEnabled = true;
            this.cbxSexo.Items.AddRange(new object[] {
            "M\t",
            "F"});
            this.cbxSexo.Location = new System.Drawing.Point(590, 36);
            this.cbxSexo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxSexo.Name = "cbxSexo";
            this.cbxSexo.Size = new System.Drawing.Size(38, 23);
            this.cbxSexo.TabIndex = 55;
            // 
            // lblMensagem
            // 
            this.lblMensagem.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblMensagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMensagem.ForeColor = System.Drawing.Color.Red;
            this.lblMensagem.Location = new System.Drawing.Point(516, 593);
            this.lblMensagem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(388, 26);
            this.lblMensagem.TabIndex = 76;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dTPDataNasc
            // 
            this.dTPDataNasc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dTPDataNasc.Location = new System.Drawing.Point(746, 37);
            this.dTPDataNasc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dTPDataNasc.Name = "dTPDataNasc";
            this.dTPDataNasc.Size = new System.Drawing.Size(93, 21);
            this.dTPDataNasc.TabIndex = 58;
            this.ttpPaciente.SetToolTip(this.dTPDataNasc, "clique na seta para exibir calendário");
            // 
            // maskeCPF
            // 
            this.maskeCPF.Location = new System.Drawing.Point(266, 34);
            this.maskeCPF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.maskeCPF.Mask = "000.000.000-00";
            this.maskeCPF.Name = "maskeCPF";
            this.maskeCPF.Size = new System.Drawing.Size(116, 21);
            this.maskeCPF.TabIndex = 53;
            // 
            // maskedRG
            // 
            this.maskedRG.Location = new System.Drawing.Point(442, 35);
            this.maskedRG.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.maskedRG.Mask = "00.000.000-0";
            this.maskedRG.Name = "maskedRG";
            this.maskedRG.Size = new System.Drawing.Size(94, 21);
            this.maskedRG.TabIndex = 54;
            // 
            // maskTxtCelular
            // 
            this.maskTxtCelular.Location = new System.Drawing.Point(83, 85);
            this.maskTxtCelular.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.maskTxtCelular.Mask = "(00)00000-0000";
            this.maskTxtCelular.Name = "maskTxtCelular";
            this.maskTxtCelular.Size = new System.Drawing.Size(116, 21);
            this.maskTxtCelular.TabIndex = 56;
            this.ttpPaciente.SetToolTip(this.maskTxtCelular, "campo obrigatório");
            // 
            // maskTxtTelFixo
            // 
            this.maskTxtTelFixo.Location = new System.Drawing.Point(291, 84);
            this.maskTxtTelFixo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.maskTxtTelFixo.Mask = "(00)0000-0000";
            this.maskTxtTelFixo.Name = "maskTxtTelFixo";
            this.maskTxtTelFixo.Size = new System.Drawing.Size(116, 21);
            this.maskTxtTelFixo.TabIndex = 57;
            // 
            // btnIncluir
            // 
            this.btnIncluir.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluir.ImageTransparentColor = System.Drawing.Color.Teal;
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(57, 25);
            this.btnIncluir.Tag = "85";
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.ToolTipText = "preencha os campos e clique para incluir um novo paciente";
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ImageTransparentColor = System.Drawing.Color.Teal;
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(61, 25);
            this.btnAlterar.Tag = "86";
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.ToolTipText = "selecione um paciente, faça as alterações desejadas e clique para alterar dados ";
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.ImageTransparentColor = System.Drawing.Color.Red;
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(63, 25);
            this.btnLimpar.Tag = "87";
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.ToolTipText = "clique para limpar dados";
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(60, 25);
            this.btnSair.Tag = "88";
            this.btnSair.Text = "&Fechar";
            this.btnSair.ToolTipText = "clique para fechar janela Cadastro Paciente";
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.LavenderBlush;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnIncluir,
            this.toolStripSeparator2,
            this.btnAlterar,
            this.toolStripSeparator4,
            this.btnLimpar,
            this.toolStripSeparator1,
            this.btnSair,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 593);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(923, 28);
            this.toolStrip1.TabIndex = 45;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // txtConvenio
            // 
            this.txtConvenio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConvenio.Location = new System.Drawing.Point(545, 87);
            this.txtConvenio.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtConvenio.Name = "txtConvenio";
            this.txtConvenio.Size = new System.Drawing.Size(84, 21);
            this.txtConvenio.TabIndex = 59;
            this.ttpPaciente.SetToolTip(this.txtConvenio, "clique para consultar ID convênio");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(447, 84);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 20);
            this.label15.TabIndex = 83;
            this.label15.Text = "IdConvênio";
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(254, 10);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(129, 28);
            this.btnFiltrar.TabIndex = 84;
            this.btnFiltrar.Text = "Pesquisar";
            this.ttpPaciente.SetToolTip(this.btnFiltrar, "clique para pesquisar");
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel1.Controls.Add(this.txtConsultarIdConvenio);
            this.panel1.Controls.Add(this.dTPDataNasc);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.maskTxtTelFixo);
            this.panel1.Controls.Add(this.txtComplemento);
            this.panel1.Controls.Add(this.txtConvenio);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtEndereco);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.maskTxtCelular);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.maskedRG);
            this.panel1.Controls.Add(this.maskeCPF);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cbxSexo);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(28, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 181);
            this.panel1.TabIndex = 84;
            // 
            // txtConsultarIdConvenio
            // 
            this.txtConsultarIdConvenio.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtConsultarIdConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsultarIdConvenio.Location = new System.Drawing.Point(651, 87);
            this.txtConsultarIdConvenio.Name = "txtConsultarIdConvenio";
            this.txtConsultarIdConvenio.Size = new System.Drawing.Size(100, 21);
            this.txtConsultarIdConvenio.TabIndex = 84;
            this.txtConsultarIdConvenio.Text = "     Consultar ID";
            this.ttpPaciente.SetToolTip(this.txtConsultarIdConvenio, "clique , realize a consulta do ID do convênio e feche a janela para retornar");
            this.txtConsultarIdConvenio.Click += new System.EventHandler(this.txtConsultarIdConvenio_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(332, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 31);
            this.label7.TabIndex = 85;
            this.label7.Text = "Clent Clínica ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.LavenderBlush;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(923, 25);
            this.toolStrip2.TabIndex = 86;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.BackColor = System.Drawing.Color.LavenderBlush;
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(134, 22);
            this.toolStripLabel1.Text = "Cadastro Paciente";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtPesquisar);
            this.panel2.Controls.Add(this.btnFiltrar);
            this.panel2.Location = new System.Drawing.Point(28, 521);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(395, 46);
            this.panel2.TabIndex = 87;
            // 
            // lbldata
            // 
            this.lbldata.AutoSize = true;
            this.lbldata.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbldata.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldata.Location = new System.Drawing.Point(336, 603);
            this.lbldata.Name = "lbldata";
            this.lbldata.Size = new System.Drawing.Size(74, 18);
            this.lbldata.TabIndex = 88;
            this.lbldata.Text = "00/00/0000";
            // 
            // lblhora
            // 
            this.lblhora.AutoSize = true;
            this.lblhora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhora.Location = new System.Drawing.Point(267, 603);
            this.lblhora.Name = "lblhora";
            this.lblhora.Size = new System.Drawing.Size(58, 18);
            this.lblhora.TabIndex = 89;
            this.lblhora.Text = "00:00:00";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(353, 288);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(171, 31);
            this.label14.TabIndex = 108;
            this.label14.Text = "Selecionar Paciente";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FormPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(923, 621);
            this.ControlBox = false;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblhora);
            this.Controls.Add(this.lbldata);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblMensagem);
            this.Controls.Add(this.dgvUsuarios);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lblProntuario);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ttpPaciente.SetToolTip(this, "clique abaixo no paciente que deseja selecionar");
            this.Load += new System.EventHandler(this.FormPaciente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtPesquisar;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblProntuario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxSexo;
        private System.Windows.Forms.Label lblMensagem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DateTimePicker dTPDataNasc;
        private System.Windows.Forms.MaskedTextBox maskeCPF;
        private System.Windows.Forms.MaskedTextBox maskedRG;
        private System.Windows.Forms.MaskedTextBox maskTxtCelular;
        private System.Windows.Forms.MaskedTextBox maskTxtTelFixo;
        private System.Windows.Forms.ToolStripButton btnIncluir;
        private System.Windows.Forms.ToolStripButton btnAlterar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnLimpar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TextBox txtConvenio;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label lbldata;
        private System.Windows.Forms.Label lblhora;
        private System.Windows.Forms.ToolTip ttpPaciente;
        private System.Windows.Forms.TextBox txtConsultarIdConvenio;
        private System.Windows.Forms.Label label14;

    }
}