﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioAtestado : Form
    {

        PacienteBll pacienteBll = new PacienteBll();
        MedicoBll medicoBll = new MedicoBll();
        ConsultaBll consultaBll = new ConsultaBll();

        string filtro;
     
        public FormRelatorioAtestado()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCRM.Text == "")
                {
                    lblMensagem.Text = "Por favor, insira o CRM.";
                }
                else
                    if (NumDias.Value == 0)
                    {
                        lblMensagem.Text = "Por favor insira os dias.";
                    }
                    else
                    {
                        Globais.tipoRelatorio = "atestado";
                        Globais.dias = Convert.ToInt32(NumDias.Value);
                        filtro = Globais.prontuario.ToString();
                        Globais.paciente = pacienteBll.Listagem(filtro).ElementAt(0);
                        Globais.consulta = consultaBll.Listagem(filtro).ElementAt(0);
                        filtro = txtCRM.Text;
                        Globais.medico = medicoBll.Listagem(filtro).ElementAt(0);
                        FormRelatorio frmRelatorio = new FormRelatorio();
                        frmRelatorio.ShowDialog();
                    }
                               
            }
            catch
            {
                lblMensagem.Text = "Não foi possível gerar atestado.";

            }

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCRM.Text = "";
            NumDias.Value = 0;
            lblMensagem.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
     
        private void txtConsultarIdConvenio_Click(object sender, EventArgs e)
        {
            FormMedico frmMedico = new FormMedico();
            Globais.ChamadaConsulta = true;
            frmMedico.ShowDialog();
            Globais.ChamadaConsulta = false;
        }
        
    }
}
