﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioMedico : Form
    {
        MedicoBll medicoBll = new MedicoBll();
        Medico medico = new Medico();
        string filtro;

        public FormRelatorioMedico()
        {
            InitializeComponent();
        }

        private void txtConsultarIdConvenio_Click(object sender, EventArgs e)
        {
            FormMedico frmMedico = new FormMedico();
            Globais.ChamadaConsulta = true;
            frmMedico.ShowDialog();
            Globais.ChamadaConsulta = false;
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCRM.Text == "")
                {
                    lblMensagem.Text = "Por favor, digite um CRM";
                }
                else
                {
                    filtro = txtCRM.Text;
                    Globais.tipoRelatorio = "medico";
                    Globais.medico = medicoBll.Listagem(filtro).ElementAt(0);
                    FormRelatorio frmRelatorio = new FormRelatorio();
                    frmRelatorio.ShowDialog();

                }
                
            }
            catch
            {
                lblMensagem.Text = "CRM inválido.";
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCRM.Text = "";
            lblMensagem.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        
    }
}
