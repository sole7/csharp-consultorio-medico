﻿namespace Clinica
{
    partial class FormConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsulta));
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.dgvConsulta = new System.Windows.Forms.DataGridView();
            this.btnAlterar = new System.Windows.Forms.ToolStripButton();
            this.btnLimpar = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnIncluir = new System.Windows.Forms.ToolStripButton();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.lblIdConsulta = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExcluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.lbldata = new System.Windows.Forms.ToolStripLabel();
            this.lblhora = new System.Windows.Forms.ToolStripLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtConsultarCRM = new System.Windows.Forms.TextBox();
            this.txtConsultaProntuario = new System.Windows.Forms.TextBox();
            this.maskHora = new System.Windows.Forms.MaskedTextBox();
            this.dtpdataRetorno = new System.Windows.Forms.DateTimePicker();
            this.dtpdataConsulta = new System.Windows.Forms.DateTimePicker();
            this.txtCid = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.txtDiagnostico = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCRM = new System.Windows.Forms.TextBox();
            this.txtExame = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtProntuario = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dTPDataAgenda = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.txtPesquisar = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.ttpConsulta = new System.Windows.Forms.ToolTip(this.components);
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // lblMensagem
            // 
            this.lblMensagem.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblMensagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMensagem.ForeColor = System.Drawing.Color.Red;
            this.lblMensagem.Location = new System.Drawing.Point(503, 571);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(388, 26);
            this.lblMensagem.TabIndex = 51;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // dgvConsulta
            // 
            this.dgvConsulta.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvConsulta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsulta.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvConsulta.Location = new System.Drawing.Point(14, 323);
            this.dgvConsulta.Name = "dgvConsulta";
            this.dgvConsulta.Size = new System.Drawing.Size(876, 158);
            this.dgvConsulta.TabIndex = 93;
            this.dgvConsulta.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsuarios_CellContentClick);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ImageTransparentColor = System.Drawing.Color.Teal;
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(61, 25);
            this.btnAlterar.Text = "&Alterar";
            this.btnAlterar.ToolTipText = "selecione uma consulta ,faça as alterações desejadas e clique para alterar dados " +
    " ";
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.ImageTransparentColor = System.Drawing.Color.Red;
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(63, 25);
            this.btnLimpar.Text = "&Limpar";
            this.btnLimpar.ToolTipText = "Limpa dados";
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Código Consulta";
            // 
            // btnIncluir
            // 
            this.btnIncluir.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluir.ImageTransparentColor = System.Drawing.Color.Teal;
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(57, 25);
            this.btnIncluir.Tag = "";
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.ToolTipText = "preencha os campos e clique para incluir um nova consulta";
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(60, 25);
            this.btnSair.Text = "&Fechar";
            this.btnSair.ToolTipText = "Fecha janela Marcação de Consulta";
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // lblIdConsulta
            // 
            this.lblIdConsulta.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblIdConsulta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIdConsulta.Location = new System.Drawing.Point(144, 54);
            this.lblIdConsulta.Name = "lblIdConsulta";
            this.lblIdConsulta.Size = new System.Drawing.Size(70, 20);
            this.lblIdConsulta.TabIndex = 47;
            this.ttpConsulta.SetToolTip(this.lblIdConsulta, "campo preenchido automático");
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.LavenderBlush;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnIncluir,
            this.toolStripSeparator2,
            this.btnAlterar,
            this.toolStripSeparator4,
            this.btnLimpar,
            this.toolStripSeparator1,
            this.btnExcluir,
            this.toolStripSeparator5,
            this.btnSair,
            this.toolStripSeparator3,
            this.lbldata,
            this.lblhora});
            this.toolStrip1.Location = new System.Drawing.Point(0, 571);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(923, 28);
            this.toolStrip1.TabIndex = 45;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // btnExcluir
            // 
            this.btnExcluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnExcluir.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluir.Image")));
            this.btnExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(59, 25);
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.ToolTipText = "selecione uma consulta e clique para excluí-la ";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // lbldata
            // 
            this.lbldata.Name = "lbldata";
            this.lbldata.Size = new System.Drawing.Size(77, 25);
            this.lbldata.Text = "dd/mm/aaaa";
            // 
            // lblhora
            // 
            this.lblhora.Name = "lblhora";
            this.lblhora.Size = new System.Drawing.Size(63, 25);
            this.lblhora.Text = "hh/mm/ss";
            this.lblhora.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel1.Controls.Add(this.txtConsultarCRM);
            this.panel1.Controls.Add(this.txtConsultaProntuario);
            this.panel1.Controls.Add(this.maskHora);
            this.panel1.Controls.Add(this.dtpdataRetorno);
            this.panel1.Controls.Add(this.dtpdataConsulta);
            this.panel1.Controls.Add(this.txtCid);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtFuncionario);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtPreco);
            this.panel1.Controls.Add(this.txtDiagnostico);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtCRM);
            this.panel1.Controls.Add(this.txtExame);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtProntuario);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(15, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 181);
            this.panel1.TabIndex = 96;
            // 
            // txtConsultarCRM
            // 
            this.txtConsultarCRM.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtConsultarCRM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsultarCRM.Location = new System.Drawing.Point(754, 35);
            this.txtConsultarCRM.Name = "txtConsultarCRM";
            this.txtConsultarCRM.Size = new System.Drawing.Size(98, 22);
            this.txtConsultarCRM.TabIndex = 119;
            this.txtConsultarCRM.Text = "Consultar CRM";
            this.ttpConsulta.SetToolTip(this.txtConsultarCRM, "clique , realize a consulta do CRM e feche a janela para retornar");
            this.txtConsultarCRM.Click += new System.EventHandler(this.txtConsultarCRM_Click);
            // 
            // txtConsultaProntuario
            // 
            this.txtConsultaProntuario.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtConsultaProntuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsultaProntuario.Location = new System.Drawing.Point(422, 36);
            this.txtConsultaProntuario.Name = "txtConsultaProntuario";
            this.txtConsultaProntuario.Size = new System.Drawing.Size(125, 22);
            this.txtConsultaProntuario.TabIndex = 118;
            this.txtConsultaProntuario.Text = "Consultar Prontuário";
            this.ttpConsulta.SetToolTip(this.txtConsultaProntuario, "clique , realize a consulta do prontuário e feche a janela para retornar");
            this.txtConsultaProntuario.Click += new System.EventHandler(this.txtConsultaProntuario_Click);
            // 
            // maskHora
            // 
            this.maskHora.Location = new System.Drawing.Point(286, 88);
            this.maskHora.Mask = "00:00:00";
            this.maskHora.Name = "maskHora";
            this.maskHora.Size = new System.Drawing.Size(47, 20);
            this.maskHora.TabIndex = 109;
            // 
            // dtpdataRetorno
            // 
            this.dtpdataRetorno.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdataRetorno.Location = new System.Drawing.Point(783, 140);
            this.dtpdataRetorno.Name = "dtpdataRetorno";
            this.dtpdataRetorno.Size = new System.Drawing.Size(80, 20);
            this.dtpdataRetorno.TabIndex = 115;
            this.ttpConsulta.SetToolTip(this.dtpdataRetorno, "clique na seta para exibir Calendário");
            // 
            // dtpdataConsulta
            // 
            this.dtpdataConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdataConsulta.Location = new System.Drawing.Point(131, 86);
            this.dtpdataConsulta.Name = "dtpdataConsulta";
            this.dtpdataConsulta.Size = new System.Drawing.Size(80, 20);
            this.dtpdataConsulta.TabIndex = 108;
            this.ttpConsulta.SetToolTip(this.dtpdataConsulta, "clique na seta para exibir Calendário");
            // 
            // txtCid
            // 
            this.txtCid.Location = new System.Drawing.Point(667, 90);
            this.txtCid.Name = "txtCid";
            this.txtCid.Size = new System.Drawing.Size(62, 20);
            this.txtCid.TabIndex = 111;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(619, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 21);
            this.label14.TabIndex = 103;
            this.label14.Text = "Cid";
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Location = new System.Drawing.Point(109, 34);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(47, 20);
            this.txtFuncionario.TabIndex = 105;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 21);
            this.label5.TabIndex = 102;
            this.label5.Text = "Funcionário";
            // 
            // txtPreco
            // 
            this.txtPreco.Location = new System.Drawing.Point(486, 91);
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(91, 20);
            this.txtPreco.TabIndex = 113;
            // 
            // txtDiagnostico
            // 
            this.txtDiagnostico.Location = new System.Drawing.Point(411, 143);
            this.txtDiagnostico.Name = "txtDiagnostico";
            this.txtDiagnostico.Size = new System.Drawing.Size(258, 20);
            this.txtDiagnostico.TabIndex = 112;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(675, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 21);
            this.label13.TabIndex = 100;
            this.label13.Text = "Data Retorno";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(354, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 21);
            this.label12.TabIndex = 99;
            this.label12.Text = "Preço Consulta";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(314, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 21);
            this.label11.TabIndex = 98;
            this.label11.Text = "Diagnóstico";
            // 
            // txtCRM
            // 
            this.txtCRM.Location = new System.Drawing.Point(658, 35);
            this.txtCRM.Name = "txtCRM";
            this.txtCRM.Size = new System.Drawing.Size(90, 20);
            this.txtCRM.TabIndex = 110;
            // 
            // txtExame
            // 
            this.txtExame.Location = new System.Drawing.Point(79, 143);
            this.txtExame.Name = "txtExame";
            this.txtExame.Size = new System.Drawing.Size(222, 20);
            this.txtExame.TabIndex = 114;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(553, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 21);
            this.label10.TabIndex = 97;
            this.label10.Text = "CRM Médico";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 21);
            this.label9.TabIndex = 96;
            this.label9.Text = "Exames";
            // 
            // txtProntuario
            // 
            this.txtProntuario.Location = new System.Drawing.Point(320, 35);
            this.txtProntuario.Name = "txtProntuario";
            this.txtProntuario.Size = new System.Drawing.Size(90, 20);
            this.txtProntuario.TabIndex = 107;
            this.ttpConsulta.SetToolTip(this.txtProntuario, "campo obrigatório");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(162, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 21);
            this.label8.TabIndex = 95;
            this.label8.Text = "Prontuário Paciente*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(229, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 21);
            this.label2.TabIndex = 94;
            this.label2.Text = "Hora*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 21);
            this.label3.TabIndex = 93;
            this.label3.Text = "Data Consulta*";
            // 
            // dTPDataAgenda
            // 
            this.dTPDataAgenda.Enabled = false;
            this.dTPDataAgenda.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dTPDataAgenda.Location = new System.Drawing.Point(769, 56);
            this.dTPDataAgenda.Name = "dTPDataAgenda";
            this.dTPDataAgenda.Size = new System.Drawing.Size(80, 20);
            this.dTPDataAgenda.TabIndex = 106;
            this.ttpConsulta.SetToolTip(this.dTPDataAgenda, "Campo preenchido automático !");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(616, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 21);
            this.label4.TabIndex = 101;
            this.label4.Text = "Data Agendamento";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel2.Controls.Add(this.btnFiltrar);
            this.panel2.Controls.Add(this.txtPesquisar);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(14, 502);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(395, 46);
            this.panel2.TabIndex = 97;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(234, 9);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(129, 28);
            this.btnFiltrar.TabIndex = 117;
            this.btnFiltrar.Text = "Pesquisar";
            this.ttpConsulta.SetToolTip(this.btnFiltrar, "Clique para pesquisar ");
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click_1);
            // 
            // txtPesquisar
            // 
            this.txtPesquisar.Location = new System.Drawing.Point(67, 14);
            this.txtPesquisar.Name = "txtPesquisar";
            this.txtPesquisar.Size = new System.Drawing.Size(145, 20);
            this.txtPesquisar.TabIndex = 116;
            this.ttpConsulta.SetToolTip(this.txtPesquisar, "Filtrar por data da consulta ou prontuário ");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 96;
            this.label6.Text = "Filtrar";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.LavenderBlush;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(923, 25);
            this.toolStrip2.TabIndex = 98;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(163, 22);
            this.toolStripLabel1.Text = "Marcação de Consulta";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(328, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 31);
            this.label7.TabIndex = 99;
            this.label7.Text = "Clent Clínica ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(354, 289);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(171, 31);
            this.label15.TabIndex = 107;
            this.label15.Text = "Selecionar Consulta";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ttpConsulta.SetToolTip(this.label15, "clique abaixo na consulta que deseja selecionar");
            // 
            // FormConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(923, 599);
            this.ControlBox = false;
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dTPDataAgenda);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblMensagem);
            this.Controls.Add(this.dgvConsulta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblIdConsulta);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormConsulta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label lblMensagem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridView dgvConsulta;
        private System.Windows.Forms.ToolStripButton btnAlterar;
        private System.Windows.Forms.ToolStripButton btnLimpar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton btnIncluir;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.Label lblIdConsulta;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel lbldata;
        private System.Windows.Forms.ToolStripLabel lblhora;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MaskedTextBox maskHora;
        private System.Windows.Forms.DateTimePicker dtpdataRetorno;
        private System.Windows.Forms.DateTimePicker dtpdataConsulta;
        private System.Windows.Forms.DateTimePicker dTPDataAgenda;
        private System.Windows.Forms.TextBox txtCid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.TextBox txtDiagnostico;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCRM;
        private System.Windows.Forms.TextBox txtExame;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtProntuario;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.TextBox txtPesquisar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripButton btnExcluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TextBox txtConsultarCRM;
        private System.Windows.Forms.TextBox txtConsultaProntuario;
        private System.Windows.Forms.ToolTip ttpConsulta;
        private System.Windows.Forms.Label label15;
    }
}