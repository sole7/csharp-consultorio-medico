﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioConsulta : Form
    {
        
        ConsultaBll consultaBll = new ConsultaBll();
        PacienteBll pacienteBll = new PacienteBll();
        MedicoBll medicoBll = new MedicoBll();
        string filtro;

        public FormRelatorioConsulta()
        {
            InitializeComponent();
        }
              

        private void btnCadastro_Click(object sender, EventArgs e)
        {
            try
            {
                Globais.tipoRelatorio = "consultas";
                filtro = dtpdataConsulta.Value.Date.ToShortDateString();
                List<ClinicaMedica.Modelo.Consulta> consulta = consultaBll.Listagem(filtro);
                FormRelatorio frmRelatorio = new FormRelatorio(consulta);
                frmRelatorio.ShowDialog();
            }
            catch
            {
                lblMensagem.Text = "Consultas não encontradas.";
            }
          
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            lblMensagem.Text = "";
        }


        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
