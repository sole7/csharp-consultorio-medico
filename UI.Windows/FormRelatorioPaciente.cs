﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioPaciente : Form
    {
        PacienteBll pacienteBll = new PacienteBll();
        ConsultaBll consultaBll = new ConsultaBll();
        MedicoBll medicoBll = new MedicoBll();

       // int prontuario;     
        string filtro;
      

        public FormRelatorioPaciente()
        {
            InitializeComponent();
        }

        private void txtConsultaProntuario_Click(object sender, EventArgs e)
        {
            FormPaciente frm = new FormPaciente();
            Globais.ChamadaConsulta = true;
            frm.ShowDialog();
            Globais.ChamadaConsulta = false;
        }



        private void btnCadastro_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtProntuario.Text == "")
                {
                    lblMensagem.Text = " Por favor digite um prontuário! ";
                }
                else
                {
                   Globais.prontuario= Convert.ToInt32(txtProntuario.Text);
                   filtro = Globais.prontuario.ToString();
                   Globais.paciente = pacienteBll.Listagem(filtro).ElementAt(0);
                   Globais.tipoRelatorio = "cadastro";
                   FormRelatorio frmRelatorio = new FormRelatorio();
                    frmRelatorio.ShowDialog();
                }

            }
            catch
            {
                lblMensagem.Text = " Prontuário inválido.";
            }

        }

        private void btnRecibo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProntuario.Text == "")
                {
                    lblMensagem.Text = " Por favor digite um prontuário! ";
                }
                else
                {
                    Globais.tipoRelatorio = "recibo";
                    Globais.prontuario = Convert.ToInt32(txtProntuario.Text);
                    filtro = Globais.prontuario.ToString();
                    Globais.paciente = pacienteBll.Listagem(filtro).ElementAt(0);
                    Globais.consulta = consultaBll.Listagem(filtro).ElementAt(0);
                              
                    FormRelatorio frmRelatorio = new FormRelatorio();
                    frmRelatorio.ShowDialog();
                }
            }
            catch
            {
                lblMensagem.Text = " Prontuário inválido para gerar recibo.";
            }
        }




        private void btnReceita_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProntuario.Text == "")
                {
                    lblMensagem.Text = " Por favor digite um prontuário! ";
                }
                else
                {
                    Globais.tipoRelatorio = "receita";
                    Globais.prontuario = Convert.ToInt32(txtProntuario.Text);
                    FormRelatorioReceita frmreceita = new FormRelatorioReceita();
                    frmreceita.ShowDialog();
                }

            }
            catch
            {

                if (txtProntuario.Text == "")
                {
                    lblMensagem.Text = " Por favor digite um prontuário! ";
                }
                else
                {

                    lblMensagem.Text = " Prontuário inválido. ";
                }

            }
        }



        private void btnExame_Click(object sender, EventArgs e)
        {
            try
            {

                Globais.tipoRelatorio = "exame";
                Globais.prontuario = Convert.ToInt32(txtProntuario.Text);
                FormRelatorioExame frmExame = new FormRelatorioExame();
                frmExame.ShowDialog();

            }
            catch
            {
                lblMensagem.Text = "Por favor, insira o prontuário.";
            }
        }
        
        

        private void btnAtestado_Click(object sender, EventArgs e)
        {

            if (txtProntuario.Text == "")
            {
                lblMensagem.Text = " Por favor digite um prontuário! ";
            }
            else
            {
                Globais.tipoRelatorio = "atestado";
                Globais.prontuario = Convert.ToInt32(txtProntuario.Text);
                FormRelatorioAtestado frmAtestado = new FormRelatorioAtestado();
                frmAtestado.ShowDialog();
            }
           
        }

      

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtProntuario.Text = "";
            lblMensagem.Text = "";
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      

    }
}
