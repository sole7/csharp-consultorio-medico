﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.Modelo;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormPaciente : Form
    {
        Paciente objPac = new Paciente();
        PacienteBll objPacBll = new PacienteBll();
        public string filtro;
       
        public FormPaciente()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                objPac.Nome = txtNome.Text;
                objPac.Sexo = cbxSexo.Text;
                objPac.Cpf =   maskeCPF.Text;
                objPac.Rg =   maskedRG.Text;
                objPac.Endereco = txtEndereco.Text;
                objPac.Complemento = txtComplemento.Text;
                objPac.Email = txtEmail.Text;
                objPac.TelFixo =    maskTxtTelFixo.Text;
                objPac.Celular =  maskTxtCelular.Text;
                objPac.DataNascimento = Convert.ToDateTime(dTPDataNasc.Value.ToShortDateString());
                objPac.Convenio =Convert.ToInt32(txtConvenio.Text);

                objPacBll.IncluirPaciente(objPac);

                lblProntuario.Text = objPac.Prontuario;

                if (objPacBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                lblMensagem.Text = objPacBll.Mensagem;

                AtualizarGrid();
            }
            catch
            {
                lblMensagem.Text = "Paciente não pode ser cadastrado ! ";
            }
        
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                objPac.Prontuario = lblProntuario.Text;
                objPac.Nome = txtNome.Text;
                objPac.Sexo = cbxSexo.Text;
                objPac.Cpf = maskeCPF.Text;
                objPac.Rg =  maskedRG.Text  ;
                objPac.Sexo = cbxSexo.Text;
                objPac.Endereco = txtEndereco.Text;
                objPac.Complemento = txtComplemento.Text;
                objPac.Email = txtEmail.Text;
                objPac.TelFixo = maskTxtTelFixo.Text;
                objPac.Celular = maskTxtCelular.Text;
                objPac.DataNascimento = Convert.ToDateTime(dTPDataNasc.Value.ToShortDateString());
                objPac.Convenio = Convert.ToInt32(txtConvenio.Text);

                objPacBll.AlterarPaciente(objPac);

                if (objPacBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                lblMensagem.Text = objPacBll.Mensagem;

                AtualizarGrid();

            }
            catch
            {
                lblMensagem.Text = "Dados não podem ser alterados !";
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnLimpar_Click(object sender, EventArgs e)
        {
           
            limpaControles(this);

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            lblProntuario.Text = dgvUsuarios[0,dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtNome.Text = dgvUsuarios[1, dgvUsuarios.CurrentRow.Index].Value.ToString();
            cbxSexo.Text = dgvUsuarios[2, dgvUsuarios.CurrentRow.Index].Value.ToString();
            maskeCPF.Text = dgvUsuarios[3, dgvUsuarios.CurrentRow.Index].Value.ToString();
            maskedRG.Text = dgvUsuarios[4,dgvUsuarios.CurrentRow.Index].Value.ToString();
            // date time picker
            dTPDataNasc.Value = Convert.ToDateTime(dgvUsuarios[5, dgvUsuarios.CurrentRow.Index].Value);
            txtEndereco.Text = dgvUsuarios[6, dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtComplemento.Text = dgvUsuarios[7,dgvUsuarios.CurrentRow.Index].Value.ToString();
            maskTxtTelFixo.Text = dgvUsuarios[8, dgvUsuarios.CurrentRow.Index].Value.ToString();
            maskTxtCelular.Text = dgvUsuarios[9, dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtEmail.Text = dgvUsuarios[10, dgvUsuarios.CurrentRow.Index].Value.ToString();
            txtConvenio.Text = dgvUsuarios[11, dgvUsuarios.CurrentRow.Index].Value.ToString();

        }

        private void FormatarGrid()
        {
            dgvUsuarios.Columns[0].HeaderText =  "Prontuário";
            dgvUsuarios.Columns[0].Width= 100;

            dgvUsuarios.Columns[1].HeaderText = "Nome";
            dgvUsuarios.Columns[1].Width= 210;
            
            dgvUsuarios.Columns[2].HeaderText = "Sexo";
            dgvUsuarios.Columns[2].Width= 30;

            dgvUsuarios.Columns[3].HeaderText = "CPF";
            dgvUsuarios.Columns[3].Width= 130;

            dgvUsuarios.Columns[4].HeaderText = "RG";
            dgvUsuarios.Columns[4].Width= 130;

            dgvUsuarios.Columns[5].HeaderText = "DtaNascimento";
            dgvUsuarios.Columns[5].Width= 100;

            dgvUsuarios.Columns[6].HeaderText = "Endereço";
            dgvUsuarios.Columns[6].Width= 210;

            dgvUsuarios.Columns[7].HeaderText = "Complemento";
            dgvUsuarios.Columns[7].Width= 180;
            
            dgvUsuarios.Columns[8].HeaderText = "Telefone";
            dgvUsuarios.Columns[8].Width= 100;

            dgvUsuarios.Columns[9].HeaderText = "Celular";
            dgvUsuarios.Columns[9].Width= 100;

            dgvUsuarios.Columns[10].HeaderText = "Email";
            dgvUsuarios.Columns[10].Width= 180;

            dgvUsuarios.Columns[11].HeaderText = "Convênio";
            dgvUsuarios.Columns[11].Width = 60;


        }

        private void AtualizarGrid()
        {
            filtro = txtPesquisar.Text ;
            try
            {
                dgvUsuarios.DataSource = objPacBll.Listagem(filtro);
            }
            catch
            {
                lblMensagem.Text = "Não foi possível realizar a pesquisa!";
            }
            finally
            {
                FormatarGrid();

            }

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {                  
            try
            {
                AtualizarGrid();                       
                limpaControles(this);
            }
            catch
            {
                lblMensagem.Text = "Pesquisa não encontrada";
            }

        }


        private void FormPaciente_Load(object sender, EventArgs e)
        {

            if (Globais.ChamadaConsulta == true)
            {
                btnAlterar.Visible = false;
                btnIncluir.Visible = false;
            }
                 AtualizarGrid();

                // Estabelecendo e exibindo a data do sistema
                string strData, strDia, strMes, strAno;
                strDia = DateTime.Now.Day.ToString("00");
                strMes = DateTime.Now.Month.ToString("00");
                strAno = DateTime.Now.Year.ToString("0000");
                strData = strDia + "/" + strMes + "/" + strAno;
                lbldata.Text = strData;

                timer1.Enabled = true;

                timer1.Interval = 1000;
              

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRelogio, strHora, strMinuto, strSegundo;
            strHora = DateTime.Now.Hour.ToString("00");
            strMinuto = DateTime.Now.Minute.ToString("00");
            strSegundo = DateTime.Now.Second.ToString("00");
            strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
            lblhora.Text = strRelogio;
        }

        private void limpaControles(Control controle)
        {
            lblMensagem.Text = "";
            lblProntuario.Text = "";
            cbxSexo.Text = "";
            dTPDataNasc.Text = "";

            foreach (Control ctrl in controle.Controls)
            {
                if (ctrl is TextBox)
                {                           //vazio
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }
                  
                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
            }

        }
            

        private void txtConsultarIdConvenio_Click(object sender, EventArgs e)
        {
            FormConvenio frm = new FormConvenio();
            Globais.ChamadaConsulta = true;
            frm.ShowDialog();
            Globais.ChamadaConsulta = false;
        }

       

    }
}
