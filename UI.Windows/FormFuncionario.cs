﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormFuncionario : Form
    {
        Funcionario objfun = new Funcionario();
        FuncionarioBll objfunBll = new FuncionarioBll();
        public string filtro;


        public FormFuncionario()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {

                objfun.Nome = txtNome.Text;
                objfun.Cpf = maskeCPF.Text;
                objfun.Rg = maskedRG.Text;
                objfun.Endereco = txtEndereco.Text;
                objfun.Complemento = txtComplemento.Text;
                objfun.Email = txtEmail.Text;
                objfun.TelFixo = maskTxtTelFixo.Text;
                objfun.Celular = maskTxtCelular.Text;


                objfunBll.IncluirFuncionario(objfun);

                lblIdUsuario.Text = objfun.IdFuncionario.ToString();

                if (objfunBll.Mensagem.Contains("cadastrado"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                lblMensagem.Text = objfunBll.Mensagem;

                AtualizarGrid();
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco ";
            }
        }





        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                objfun.IdFuncionario = Convert.ToInt32(lblIdUsuario.Text);
                objfun.Nome = txtNome.Text;
                objfun.Cpf = maskeCPF.Text;
                objfun.Rg = maskedRG.Text;
                objfun.Endereco = txtEndereco.Text;
                objfun.Complemento = txtComplemento.Text;
                objfun.Email = txtEmail.Text;
                objfun.TelFixo = maskTxtTelFixo.Text;
                objfun.Celular = maskTxtCelular.Text;

                objfunBll.AlterarFuncionario(objfun);

                if (objfunBll.Mensagem.Contains("sucesso"))
                    label2.ForeColor = Color.Blue;
                else
                    label2.ForeColor = Color.Red;
                
                lblMensagem.Text = objfunBll.Mensagem;

                AtualizarGrid();
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
            }
            
        }


        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaControles(this);
        }


        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                lblIdUsuario.Text = dgvUsuarios[0, dgvUsuarios.CurrentRow.Index].Value.ToString();
                txtNome.Text = dgvUsuarios[1, dgvUsuarios.CurrentRow.Index].Value.ToString();
                maskeCPF.Text = dgvUsuarios[2, dgvUsuarios.CurrentRow.Index].Value.ToString();
                maskedRG.Text = dgvUsuarios[3, dgvUsuarios.CurrentRow.Index].Value.ToString();
                maskTxtCelular.Text = dgvUsuarios[8, dgvUsuarios.CurrentRow.Index].Value.ToString();
                maskTxtTelFixo.Text = dgvUsuarios[7, dgvUsuarios.CurrentRow.Index].Value.ToString();
                txtEmail.Text = dgvUsuarios[6, dgvUsuarios.CurrentRow.Index].Value.ToString();
                txtEndereco.Text = dgvUsuarios[4, dgvUsuarios.CurrentRow.Index].Value.ToString();
                txtComplemento.Text = dgvUsuarios[5, dgvUsuarios.CurrentRow.Index].Value.ToString();
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
            }
        }


        private void FormatarGrid()
        {
            try
            {

                dgvUsuarios.Columns[0].HeaderText = ("Código");
                dgvUsuarios.Columns[0].Width = 60;

                dgvUsuarios.Columns[1].HeaderText = ("Nome");
                dgvUsuarios.Columns[1].Width = 180;

                dgvUsuarios.Columns[2].HeaderText = ("CPF");
                dgvUsuarios.Columns[2].Width = 120;

                dgvUsuarios.Columns[3].HeaderText = ("RG");
                dgvUsuarios.Columns[3].Width = 120;

                dgvUsuarios.Columns[4].HeaderText = ("Endereço");
                dgvUsuarios.Columns[4].Width = 240;

                dgvUsuarios.Columns[5].HeaderText = ("Complemento");
                dgvUsuarios.Columns[5].Width = 180;

                dgvUsuarios.Columns[6].HeaderText = ("Email");
                dgvUsuarios.Columns[6].Width = 120;

                dgvUsuarios.Columns[7].HeaderText = ("Telefone");
                dgvUsuarios.Columns[7].Width = 100;

                dgvUsuarios.Columns[8].HeaderText = ("Celular");
                dgvUsuarios.Columns[8].Width = 100;
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
            }


        }


        private void AtualizarGrid()
        {
            filtro = txtPesquisar.Text;
            try
            {
                dgvUsuarios.DataSource = objfunBll.Listagem(filtro);
            }
            catch
            {
                lblMensagem.Text = "Nao foi possível a conexão com o banco de dados!";
            }
            finally
            {
                FormatarGrid();
            }
        }


        private void FormFuncionario_Load_1(object sender, EventArgs e)
        {
            try 
            {
                 AtualizarGrid();

                // Estabelecendo e exibindo a data do sistema
                string strData, strDia, strMes, strAno;
                strDia = DateTime.Now.Day.ToString("00");
                strMes = DateTime.Now.Month.ToString("00");
                strAno = DateTime.Now.Year.ToString("0000");
                strData = strDia + "/" + strMes + "/" + strAno;
                lblData.Text = strData;

                timer1.Enabled = true;

                timer1.Interval = 1000;
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRelogio, strHora, strMinuto, strSegundo;
            strHora = DateTime.Now.Hour.ToString("00");
            strMinuto = DateTime.Now.Minute.ToString("00");
            strSegundo = DateTime.Now.Second.ToString("00");
            strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
            lblhora.Text = strRelogio;
        }


        private void limpaControles(Control controle)
        {
            lblMensagem.Text = "";
            lblIdUsuario.Text = "";

            foreach (Control ctrl in controle.Controls)
            {
                
                if (ctrl is TextBox)
                {                          
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }
                  
                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
                
            }

        }

        private void btnFiltrar_Click_1(object sender, EventArgs e)
        {            
            try
            {
                AtualizarGrid();
                limpaControles(this);

            }
            catch
            {
                lblMensagem.Text = "Pesquisa não encontrada";
            }

            
        }
        
      
        }

        
    }

