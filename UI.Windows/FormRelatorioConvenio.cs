﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormRelatorioConvenio : Form
    {
        ConvenioBll conveniobll = new ConvenioBll();
        string filtro;
        
        public FormRelatorioConvenio()
        {
            InitializeComponent();
        }


        private void txtConsultarIdConvenio_Click(object sender, EventArgs e)
        {
            FormConvenio frmConvenio = new FormConvenio();
            Globais.ChamadaConsulta = true;
            frmConvenio.ShowDialog();
            Globais.ChamadaConsulta = false;

        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtConvenio.Text == "")
                {
                    lblMensagem.Text = "Por favor, digite um ID.";
                }
                else
                {
                    filtro = txtConvenio.Text;
                    Globais.tipoRelatorio = "convenio";
                    Globais.convenio = conveniobll.Listagem(filtro).ElementAt(0);
                    FormRelatorio frmrelatorio = new FormRelatorio();
                    frmrelatorio.ShowDialog();
                }

              
            }
            catch
            {
                lblMensagem.Text = "ID inválido.";
            }
        }


        private void btnLimpar_Click(object sender, EventArgs e)
        {
            lblMensagem.Text = "";
            txtConvenio.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
           
    
    }
}
