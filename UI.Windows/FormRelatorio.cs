﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.Modelo;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormRelatorio : Form
    {     
        string strData, strDia, strMes, strAno;
        List<Consulta> consultas = null;
        List<Paciente> pacientes = null;

        public FormRelatorio()
        {
            InitializeComponent();
                 
        }

        public FormRelatorio(List<Consulta> consulta)
        {
            consultas = consulta;
            InitializeComponent();
        }
        public FormRelatorio(List<Consulta> consulta, List<Paciente> paciente)
        {
            consultas = consulta;
            pacientes = paciente;
            InitializeComponent();
        }
           

        private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string strDados = "";

            Graphics objImpressao = e.Graphics;
                        
            if (Globais.tipoRelatorio == "cadastro")
           {

               objImpressao.DrawString("RELATÓRIO FICHA PACIENTE", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50 );
                strDados = "" + Chr(10) + Chr(10) + Chr(10);
                strDados += "Nome " + Globais.paciente.Nome + Chr(9) + Chr(9) + Chr(9) + Chr(9);
                strDados += "Prontuário " + Globais.paciente.Prontuario.ToString() + Chr(10) + Chr(10);
                strDados += "Data Nascimento " + Globais.paciente.DataNascimento + Chr(9) + Chr(9) + Chr(9);
                strDados += "Sexo " + Globais.paciente.Sexo + Chr(10) + Chr(10);
                strDados += "RG " + Globais.paciente.Rg + Chr(9) + Chr(9) + Chr(9);
                strDados += "CPF " + Globais.paciente.Cpf + Chr(10) + Chr(10);      
                strDados += "Endereço: " + Globais.paciente.Endereco + Chr(9);
                strDados += "Complemento: " + Globais.paciente.Complemento + Chr(10) + Chr(10);
                strDados += "Telefone: " + Globais.paciente.TelFixo + Chr(9) + Chr(9) + Chr(9);
                strDados += "Celular: " + Globais.paciente.Celular + Chr(10) + Chr(10);
                strDados += "Email: " + Globais.paciente.Email + Chr(10) + Chr(10);

                objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);
               
            }
           else if (Globais.tipoRelatorio == "recibo")
           {
               objImpressao.DrawString(" RECIBO ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);
               strDados = "" + Chr(10) + Chr(10) + Chr(10);
               strDados += "RECIBO"+Chr(9)+"VALOR R$" + Globais.consulta.Preco.ToString() + Chr(10) + Chr(10);
               strDados = "Recebemos de : " + Globais.paciente.Nome + Chr(9);
               strDados += "CPF: " + Globais.paciente.Cpf.ToString() + Chr(10) + Chr(10);
               strDados += "a quantia de R$ " + Globais.consulta.Preco.ToString() + Chr(10) + Chr(10);
               strDados += "referente a consulta de ID:  " + Globais.consulta.IdConsulta.ToString() + Chr(9);
               strDados += "CRM médico: " + Globais.consulta.CrmMedico + Chr(10) + Chr(10);
               strDados += "Data: " + strData;

               objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);
             
           }
           else
               if(Globais.tipoRelatorio == "convenio")
               {

                   objImpressao.DrawString(" CONVÊNIO ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);
                   strDados = "Número: " + Globais.convenio.IdConvenio.ToString() + Chr(10) + Chr(10);
                   strDados += "CNPJ: " + Globais.convenio.Cnpj + Chr(10) + Chr(10);
                   strDados += "Nome: " + Globais.convenio.Nome + Chr(10) + Chr(10);
                   strDados += "Contato: " + Globais.convenio.Contato + Chr(10) + Chr(10);
                   strDados += "Telefone: " + Globais.convenio.Telefone + Chr(10) + Chr(10);

                   objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);


                   
               }
               else
                   if (Globais.tipoRelatorio == "consultaPaciente")
                   {
                       objImpressao.DrawString(" CONSULTA PACIENTE ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);
                       foreach (Consulta c in consultas)
                       {
                           strDados += "" + Chr(10) + Chr(10) + Chr(10);
                           strDados += "ID Consulta: " + c.IdConsulta.ToString() + Chr(9) + Chr(9);
                           strDados += "Data: " + c.DataConsulta + Chr(9);
                           strDados += "Hora: " + c.Hora + Chr(10) + Chr(10);
                           strDados += "Nome: " +pacientes.ElementAt(0).Nome + Chr(9) + Chr(9);
                           strDados += "Prontuário: " + c.Prontuario.ToString() + Chr(9);
                           strDados += "Celular: " + pacientes.ElementAt(0).Celular + Chr(10) + Chr(10);
                           strDados += "CRM do médico:" + c.CrmMedico + Chr(9);
                           strDados += "ID Convênio: " + pacientes.ElementAt(0).Convenio + Chr(10) + Chr(10);
                           strDados += "Retorno " + c.DataRetorno;

                           
                       }
                       objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);
                                                           
                   }
                   else
                       if(Globais.tipoRelatorio == "consultas")
                       {
                           objImpressao.DrawString(" CONSULTAS ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);
                           foreach (Consulta c in consultas)
                           {
                               strDados += "" + Chr(10) + Chr(10) + Chr(10);
                               strDados += "DATA " + consultas.ElementAt(0).DataConsulta + Chr(10) + Chr(10);
                               strDados += "ID Consulta: " + consultas.ElementAt(0).IdConsulta.ToString() + Chr(10) + Chr(10);
                               strDados += "Prontuário: " + consultas.ElementAt(0).Prontuario.ToString() + Chr(10) + Chr(10);
                               strDados += "CRM do médico:" + c.CrmMedico + Chr(9);
                               strDados += "Hora: " + consultas.ElementAt(0).Hora + Chr(10) + Chr(10);
                               strDados += "Prontuário: " + consultas.ElementAt(0).Prontuario.ToString() + Chr(9);
                               strDados += "" + Chr(10) + Chr(10) + Chr(10);
                           }
                           objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);
                       }
                       else
                           if(Globais.tipoRelatorio == "medico")
                           {
                               objImpressao.DrawString(" MÉDICO ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);
                               strDados = "Médico: " + Globais.medico.Nome + Chr(10) + Chr(10);
                               strDados += "CRM: " + Globais.medico.Crm + Chr(10) + Chr(10);
                               strDados += " especialidade: " + Globais.medico.Especialidade + Chr(10) + Chr(10);
                               strDados += "endereço: " + Globais.medico.Endereco + Chr(10) + Chr(10);
                               strDados += "complemento: " + Globais.medico.Complemento + Chr(10) + Chr(10);
                               strDados += "telefone: " + Globais.medico.Telefone + Chr(10) + Chr(10);
                               strDados += "celular: " + Globais.medico.Celular + Chr(10) + Chr(10);
                               strDados += "email: " + Globais.medico.Email + Chr(10) + Chr(10);

                               objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);

                           }
           else
              if(Globais.tipoRelatorio == "receita")
              {
                  objImpressao.DrawString(" RECEITUÁRIO MÉDICO ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50 + Chr(10) + Chr(10));

                   strDados = "" + Chr(10) + Chr(10) + Chr(10);
                   strDados += "Clínica: CLENT " + Chr(9) + Chr(9);
                   strDados += "Data: " + strData + Chr(10) + Chr(10);
                   strDados += "Paciente " + Globais.paciente.Nome + Chr(9) + Chr(9);
                   strDados += "Prontuário " + Globais.paciente.Prontuario + Chr(9) + Chr(9);
                   strDados += "ID do Plano:  " + Globais.paciente.Convenio + Chr(10) + Chr(10);
                   strDados += "Endereço " + Globais.paciente.Endereco + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10);
                   strDados += Globais.receituário + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10);
                   strDados += "" + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(10);
                   strDados += "Drº(ª) " + Globais.medico.Nome + Chr(10);
                   strDados += "CRM " + Globais.medico.Crm + Chr(10) + Chr(10) + Chr(10);
                   
                   
                  objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);
                 
              }
  
              else
                  if (Globais.tipoRelatorio == "atestado")
                  {
                      objImpressao.DrawString(" ATESTADO MÉDICO ", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);

                      strDados = "" + Chr(10) + Chr(10) + Chr(10) + Chr(10);
                      strDados += "Atesto que o SRº(ª)  " + Globais.paciente.Nome + Chr(10) + Chr(10);
                      strDados += "necessita de  " + Globais.dias + " dia(s) de afastamento do trabalho por motivo  " + Chr(10) + Chr(10);
                      strDados += " de doença, " + Chr(9) + "CID: " + Globais.consulta.Cid + Chr(10) + Chr(10);
                      strDados += "a partir da data de " + strData + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10);
                      strDados += "" + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(10);
                      strDados += "Drº(ª) " + Globais.medico.Nome + Chr(10) ;
                      strDados += "CRM " + Globais.medico.Crm;

                      objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);

                  }
            else if (Globais.tipoRelatorio == "exame")
           {
               objImpressao.DrawString(" EXAME MÉDICO", new System.Drawing.Font("Times New Roman", 14, FontStyle.Bold), Brushes.Black, 50, 50);

               strDados = "" + Chr(10) + Chr(10) + Chr(10) + Chr(10);
               strDados += "Clínica: CLENT " + Chr(9) + Chr(9);
               strDados += "Data: " + strData + Chr(10)+ Chr(10);
               strDados += "Paciente: " + Globais.paciente.Nome + Chr(9) + Chr(9);
               strDados += "Prontuário: " + Globais.paciente.Prontuario + Chr(9) + Chr(9);
               strDados +=  "ID do Plano:  " + Globais.paciente.Convenio + Chr(10) + Chr(10);
               strDados +=  Globais.exame + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10);
               strDados += "" + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(95) + Chr(10);
               strDados += "Drº(ª) " + Globais.medico.Nome + Chr(10);
               strDados += " CRM : " + Globais.medico.Crm + Chr(10) + Chr(10);

                      
               objImpressao.DrawString(strDados, new System.Drawing.Font("Arial", 12, FontStyle.Bold), Brushes.Black, 50, 100);


           }
         
            
        }


        private char Chr(int codigo)
        {
            return (char)codigo;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (printDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                printDoc.DocumentName = "Ficha Paciente";
                printDoc.Print();
            }
            else
            {
                MessageBox.Show("Falha na impressão!", "Erro");
            }
        }

        private void btnVisualizarImpressao_Click(object sender, EventArgs e)
        {
           printPreview.ShowDialog();
        }

        private void FormRelatorio_Load(object sender, EventArgs e)
        {           
            strDia = DateTime.Now.Day.ToString("00");
            strMes = DateTime.Now.Month.ToString("00");
            strAno = DateTime.Now.Year.ToString("0000");
            strData = strDia + "/" + strMes + "/" + strAno;
           
       
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
    }
}
