﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormRelatorioConsPaciente : Form
    {

        PacienteBll pacienteBll = new PacienteBll();
        ConsultaBll consultaBll = new  ConsultaBll();
        MedicoBll   medicoBll = new MedicoBll();
 
        string filtro;

        public FormRelatorioConsPaciente()
        {
            InitializeComponent();
        }

        private void txtConsultaProntuario_Click(object sender, EventArgs e)
        {
            FormPaciente frm = new FormPaciente();
            Globais.ChamadaConsulta = true;
            frm.ShowDialog();
            Globais.ChamadaConsulta = false;
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProntuario.Text == "")
                {
                    lblMensagem.Text = "Por favor, digite um prontuário.";
                }
                else
                {
                    Globais.tipoRelatorio = "consultaPaciente";
                    filtro = txtProntuario.Text;

                    List<ClinicaMedica.Modelo.Consulta> consulta = consultaBll.Listagem(filtro);
                    List<ClinicaMedica.Modelo.Paciente> paciente = pacienteBll.Listagem(filtro);
                    FormRelatorio frmRelatorio = new FormRelatorio(consulta, paciente);
                    frmRelatorio.ShowDialog();
                }
            }
            catch
            {
                lblMensagem.Text = "Prontuário inválido.";
            }
            
                 
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtProntuario.Text = "";
            lblMensagem.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      
        
    }
}
