﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.Modelo;
using ClinicaMedica.BLL;

namespace Clinica
{
    public partial class FormLogin : Form
    {
        UsuarioBll objbll = new UsuarioBll();
        Usuario objUsuario = new Usuario();
        
        public FormLogin()
        {
            InitializeComponent();            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            bool autentica = false;
            try
            {              
                objUsuario.Nome = txtUsuario.Text;
                objUsuario.Senha = txtSenha.Text;
                objUsuario.Tipo = cboTipo.Text;

               autentica =  objbll.Login(objUsuario);
               if (autentica)
               {
                   this.Hide();
                   Globais.tipoUsuario = objUsuario.Tipo;
                   FormPrincipal frm = new FormPrincipal();

                   frm.ShowDialog();
               }
               else 
               {
                   lblMensagem.Text = " Dados de login inválidos!";
                   // limpaControles();
               }

            }
            catch
            {
                lblMensagem.Text = "Desculpe, não foi possível a conexão com o banco!";
            }

        }

        private void limpaControles()
        {
            txtUsuario.Text = " ";
            txtSenha.Text = " ";
            cboTipo.Text = " ";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

     

       
    }
}
