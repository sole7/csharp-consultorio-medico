﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioReceita : Form
    {
        MedicoBll medicobll = new MedicoBll();
        PacienteBll pacienteBll = new PacienteBll();
        string filtro;

        public FormRelatorioReceita()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            if (txtCRM.Text == "")
            {
                lblMensagem.Text = "Por favor, digite o CRM";
            }
              
                else
                {
                    Globais.receituário = txtReceituario.Text;
                    Globais.tipoRelatorio = "receita";
                    filtro = txtCRM.Text;
                    Globais.medico = medicobll.Listagem(filtro).ElementAt(0);
                    filtro = Globais.prontuario.ToString();
                    Globais.paciente = pacienteBll.Listagem(filtro).ElementAt(0);

                    FormRelatorio frmrelatorio = new FormRelatorio();
                    frmrelatorio.ShowDialog();


                }
        }
             

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCRM.Text = "";
            lblMensagem.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormReceita_Load(object sender, EventArgs e)
        {
            txtReceituario.Focus();
        }

        private void txtConsultarIdConvenio_Click(object sender, EventArgs e)
        {
            FormMedico frmMedico = new FormMedico();
            Globais.ChamadaConsulta = true;
            frmMedico.ShowDialog();
            Globais.ChamadaConsulta = false;
        }
       
    }
}
