﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormRelatorioExame : Form
    {
        ConsultaBll consultaBll = new ConsultaBll();
        PacienteBll pacienteBll = new PacienteBll();
        MedicoBll medicobll = new MedicoBll();
        ConvenioBll convenioBll = new ConvenioBll();

        string filtro;

        public FormRelatorioExame()
        {
            InitializeComponent();
        }
        
      
        private void btnGerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCRM.Text == "")
                {
                    lblMensagem.Text = "Por favor insira o CRM";
                }                         
                else
                {
                    Globais.exame = txtExame.Text;
                    Globais.tipoRelatorio = "exame";
                    filtro = Globais.prontuario.ToString();
                    Globais.paciente = pacienteBll.Listagem(filtro).ElementAt(0);
                    filtro = txtCRM.Text;
                    Globais.medico = medicobll.Listagem(filtro).ElementAt(0);
                                 
                  
                    FormRelatorio frmRelatorio = new FormRelatorio();
                    frmRelatorio.ShowDialog();
                }
                       
            }
            catch
            {
                lblMensagem.Text = "Não foi possível gerar exame.";
            }
        }

        private void txtConsultarCRM_Click(object sender, EventArgs e)
        {
            Globais.ChamadaConsulta = false;
            FormMedico frmMedico = new FormMedico();
            Globais.ChamadaConsulta = true;
            frmMedico.ShowDialog();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCRM.Text = "";
            lblMensagem.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

  


     

       
    }
}
