﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Clinica
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            if (Globais.tipoUsuario == "usuario")
            {
               
                usuáriosToolStripMenuItem1.Visible = false;
            }
            else if(Globais.tipoUsuario == "medico")
            {
                marcarConsultaToolStripMenuItem.Visible = false;
                médicosToolStripMenuItem.Visible = false;
                funcionariosToolStripMenuItem.Visible = false;
                convenioToolStripMenuItem.Visible = false;
                usuáriosToolStripMenuItem1.Visible = false;
                marcarConsultaToolStripMenuItem.Visible = false;

            }

            // Estabelecendo e exibindo a data do sistema
            string strData, strDia, strMes, strAno;
            strDia = DateTime.Now.Day.ToString("00");
            strMes = DateTime.Now.Month.ToString("00");
            strAno = DateTime.Now.Year.ToString("0000");
            strData = strDia + "/" + strMes + "/" + strAno;
            lblData.Text = strData;

            timer1.Enabled = true;

            timer1.Interval = 1000;

        }

              private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

              private void timer1_Tick(object sender, EventArgs e)
              {
                  string strRelogio, strHora, strMinuto, strSegundo;
                  strHora = DateTime.Now.Hour.ToString("00");
                  strMinuto = DateTime.Now.Minute.ToString("00");
                  strSegundo = DateTime.Now.Second.ToString("00");
                  strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
                  lblhora.Text = strRelogio;
              }

              private void pacientesToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormPaciente frm = new FormPaciente();
                  frm.ShowDialog();

              }

              private void médicosToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormMedico frm = new FormMedico();
                  frm.ShowDialog();
              }

              private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormFuncionario frm = new FormFuncionario();
                  frm.ShowDialog();
              }

              private void usuáriosToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormConvenio frm = new FormConvenio();
                  frm.ShowDialog();
              }

              private void marcarConsultaToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormConsulta frm = new FormConsulta();
                  frm.Show();
              }

              private void usuáriosToolStripMenuItem1_Click(object sender, EventArgs e)
              {
                  FormUsuario formUsuario = new FormUsuario();
                  formUsuario.ShowDialog();
              }

              private void MenuPaciente_Click(object sender, EventArgs e)
              {
                  FormRelatorioPaciente formRelPac = new FormRelatorioPaciente();
                  formRelPac.ShowDialog();
              }

              private void MenuConvenio_Click(object sender, EventArgs e)
              {
                  FormRelatorioConvenio frmConvenio = new FormRelatorioConvenio();
                  frmConvenio.ShowDialog();
              }

              private void consultaPacienteToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormRelatorioConsPaciente frmConsulta = new FormRelatorioConsPaciente();
                  frmConsulta.ShowDialog();
              }

              private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormRelatorioConsulta frmConsulta = new FormRelatorioConsulta();
                  frmConsulta.ShowDialog();
              }

              private void médicoToolStripMenuItem_Click(object sender, EventArgs e)
              {
                  FormRelatorioMedico frmMedico = new FormRelatorioMedico();
                  frmMedico.ShowDialog();
              }

              
    }
}
