﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormConsulta : Form
    {
        ConsultaBll objConBll = new ConsultaBll();
        Consulta objConsul = new Consulta();
        string filtro;
 

        public FormConsulta()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                objConsul.DataConsulta = Convert.ToDateTime(dtpdataConsulta.Value.ToShortDateString());
                objConsul.Hora =  maskHora.Text;
                objConsul.Preco = float.Parse(txtPreco.Text);
                objConsul.Cid = txtCid.Text;
                objConsul.DataRetorno = dtpdataRetorno.Value;
                objConsul.DataAgendamento = dTPDataAgenda.Value;
                objConsul.Exame = txtExame.Text;
                objConsul.Diagnostico = txtDiagnostico.Text;
                objConsul.CrmMedico = txtCRM.Text.ToString();
                objConsul.Prontuario = Convert.ToInt32(txtProntuario.Text);
                objConsul.IdFuncionario = Convert.ToInt32(txtFuncionario.Text);
            
                
                objConBll.MarcarConsulta(objConsul);

                lblIdConsulta.Text = objConsul.IdConsulta.ToString();

                if (objConBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                 lblMensagem.Text = objConBll.Mensagem;

                AtualizarGrid();

            }
            catch
            {
                
                lblMensagem.Text = "Não foi possível incluir consulta !";
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                objConsul.IdConsulta = Convert.ToInt32(lblIdConsulta.Text);
                objConsul.DataConsulta = Convert.ToDateTime(dtpdataConsulta.Value.ToShortDateString());
                objConsul.Hora = maskHora.Text;
                objConsul.Preco = float.Parse(txtPreco.Text);
                objConsul.Cid = txtCid.Text;
                objConsul.DataRetorno = dtpdataRetorno.Value;
                objConsul.DataAgendamento = dTPDataAgenda.Value;
                objConsul.Exame = txtExame.Text;
                objConsul.Diagnostico = txtDiagnostico.Text;
                objConsul.CrmMedico = txtCRM.Text.ToString();
                objConsul.Prontuario = Convert.ToInt32(txtProntuario.Text);
                objConsul.IdFuncionario = Convert.ToInt32(txtFuncionario.Text);

                objConBll.AlterarConsulta(objConsul);

                if (objConBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                lblMensagem.Text = objConBll.Mensagem;

                AtualizarGrid();

            }
            catch
            {
                lblMensagem.Text = "Não foi possível alterar consulta!";
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                objConsul.IdConsulta = Convert.ToInt32(lblIdConsulta.Text);
                               
                    objConBll.ExcluirConsulta(objConsul);
                    lblMensagem.Text = objConBll.Mensagem;
                    AtualizarGrid();
                

            }
            catch
            {
                lblMensagem.Text = objConBll.Mensagem;
            }

        }



        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaControles(this);
        }



        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtFuncionario.Text = dgvConsulta[0, dgvConsulta.CurrentRow.Index].Value.ToString();
            dTPDataAgenda.Text = dgvConsulta[1, dgvConsulta.CurrentRow.Index].Value.ToString();
            txtProntuario.Text = dgvConsulta[2, dgvConsulta.CurrentRow.Index].Value.ToString();
            lblIdConsulta.Text = dgvConsulta[3,dgvConsulta.CurrentRow.Index].Value.ToString();
            txtCRM.Text = dgvConsulta[4, dgvConsulta.CurrentRow.Index].Value.ToString();
            dtpdataConsulta.Text = dgvConsulta[5,dgvConsulta.CurrentRow.Index].Value.ToString();
            maskHora.Text = dgvConsulta[6, dgvConsulta.CurrentRow.Index].Value.ToString();
            txtPreco.Text = dgvConsulta[7, dgvConsulta.CurrentRow.Index].Value.ToString();         
            txtDiagnostico.Text = dgvConsulta[8, dgvConsulta.CurrentRow.Index].Value.ToString();
            txtCid.Text = dgvConsulta[9, dgvConsulta.CurrentRow.Index].Value.ToString();
            txtExame.Text = dgvConsulta[10, dgvConsulta.CurrentRow.Index].Value.ToString();
            dtpdataRetorno.Text = dgvConsulta[11, dgvConsulta.CurrentRow.Index].Value.ToString();
                   
           
          
        }


        private void FormatarGrid()
        {
            dgvConsulta.Columns[3].HeaderText = " IdConsulta";
            dgvConsulta.Columns[3].Width = 60 ;

            
            dgvConsulta.Columns[5].HeaderText = " DataConsulta";
            dgvConsulta.Columns[5].Width = 100 ;

            
            dgvConsulta.Columns[6].HeaderText = "Hora";
            dgvConsulta.Columns[6].Width = 100 ;

            
            dgvConsulta.Columns[7].HeaderText = "Preço";
            dgvConsulta.Columns[7].Width = 100;

            
            dgvConsulta.Columns[9].HeaderText = "Cid";
            dgvConsulta.Columns[9].Width = 100 ;

            
            dgvConsulta.Columns[11].HeaderText = "DataRetorno";
            dgvConsulta.Columns[11].Width = 100 ;

            
            dgvConsulta.Columns[1].HeaderText = "DataAgendamento";
            dgvConsulta.Columns[1].Width = 100  ;

            
            dgvConsulta.Columns[10].HeaderText = "Exames ";
            dgvConsulta.Columns[10].Width =  200 ;

            
            dgvConsulta.Columns[8].HeaderText = " Diagnóstico ";
            dgvConsulta.Columns[8].Width = 160 ;

            
            dgvConsulta.Columns[4].HeaderText = " CRM ";
            dgvConsulta.Columns[4].Width = 100 ;

            
            dgvConsulta.Columns[2].HeaderText = " Prontuário";
            dgvConsulta.Columns[2].Width = 100 ;

             dgvConsulta.Columns[0].HeaderText = "IdFuncionário";
            dgvConsulta.Columns[0].Width = 60 ;
  
  
        }

        private void AtualizarGrid()
        {
            filtro = txtPesquisar.Text;
            try
            {
                dgvConsulta.DataSource=objConBll.Listagem(filtro);
            }
            catch
            {
                lblMensagem.Text = "Pesquisa nao encontrada!";
            }
            finally
            {
                FormatarGrid();
            }

        }

       


        private void FormConsulta_Load(object sender, EventArgs e)
        {
           
            AtualizarGrid();
            txtFuncionario.Focus();
          

            // Estabelecendo e exibindo a data do sistema
            string strData, strDia, strMes, strAno;
            strDia = DateTime.Now.Day.ToString("00");
            strMes = DateTime.Now.Month.ToString("00");
            strAno = DateTime.Now.Year.ToString("0000");
            strData = strDia + "/" + strMes + "/" + strAno;
            lbldata.Text = strData;

            timer1.Enabled = true;

            timer1.Interval = 1000;
              
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRelogio, strHora, strMinuto, strSegundo;
            strHora = DateTime.Now.Hour.ToString("00");
            strMinuto = DateTime.Now.Minute.ToString("00");
            strSegundo = DateTime.Now.Second.ToString("00");
            strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
            lblhora.Text = strRelogio;
        }


        private void limpaControles(Control controle)
        {
            dTPDataAgenda.Text = "";
            dtpdataConsulta.Text = "";
            dtpdataRetorno.Text = "";
            lblIdConsulta.Text = "";
            lblMensagem.Text = "";
          
            
            foreach (Control ctrl in controle.Controls)
            {
                if (ctrl is TextBox)
                {                           //vazio
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }

                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
            }

            txtConsultaProntuario.Text = "Consultar Prontuário";
            txtConsultarCRM.Text = "Consultar CRM";
        }
             
      
      
        private void txtConsultaProntuario_Click(object sender, EventArgs e)
        {
            FormPaciente frm = new FormPaciente();
            Globais.ChamadaConsulta = true;
            frm.ShowDialog();
            Globais.ChamadaConsulta = false;
        }

        private void txtConsultarCRM_Click(object sender, EventArgs e)
        {
            FormMedico frm = new FormMedico();
            Globais.ChamadaConsulta = true;
            frm.ShowDialog();
            Globais.ChamadaConsulta = false;

        }

        private void btnFiltrar_Click_1(object sender, EventArgs e)
        {

            filtro = txtPesquisar.Text;

            try
            {
                dgvConsulta.DataSource = objConBll.Listagem(filtro);
            }
            catch
            {
                lblMensagem.Text = "Pesquisa não encontrada !"; ;
            }
        }

       
    }
}
