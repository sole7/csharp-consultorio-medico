﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClinicaMedica.BLL;
using ClinicaMedica.Modelo;

namespace Clinica
{
    public partial class FormMedico : Form
    {
        Medico objMed = new Medico();
        MedicoBll objMedBll = new MedicoBll();
        string filtro;

        public FormMedico()
        {
            InitializeComponent();
            
        }

        public void btnIncluir_Click(object sender, EventArgs e)
        {

            try
            {
                objMed.Crm = txtCrm.Text;
                objMed.Nome = txtNome.Text;
                objMed.Especialidade = txtEspecialidade.Text;
                objMed.Endereco = txtEndereco.Text;
                objMed.Complemento = txtComplemento.Text;
                objMed.Email = txtEmail.Text;
                objMed.Celular = maskTxtCelular.Text;
                objMed.Telefone = maskTxtTelFixo.Text;

                objMedBll.IncluirMedico(objMed);

                if (objMedBll.Mensagem.Contains("sucesso"))                
                    lblMensagem.ForeColor = Color.Blue;                
                else                
                    lblMensagem.ForeColor = Color.Red;
                
                lblMensagem.Text = objMedBll.Mensagem;

                AtualizarGrid();
               
            }
            catch 
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
               
            }
            
      
        }


        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                
                objMed.Crm = txtCrm.Text;
                objMed.Nome = txtNome.Text;
                objMed.Especialidade = txtEspecialidade.Text;
                objMed.Endereco = txtEndereco.Text;
                objMed.Complemento = txtComplemento.Text;
                objMed.Email = txtEmail.Text;
                objMed.Celular = maskTxtCelular.Text;
                objMed.Telefone = maskTxtTelFixo.Text;
                
                objMedBll.AlterarMedico(objMed);

                if (objMedBll.Mensagem.Contains("sucesso"))
                    lblMensagem.ForeColor = Color.Blue;
                else
                    lblMensagem.ForeColor = Color.Red;

                lblMensagem.Text = objMedBll.Mensagem;

                AtualizarGrid();
                      
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco! ";
            }

        }
          

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaControles(this);
        }


        private void dgvMedico_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                txtCrm.Text = dgvMedico[0, dgvMedico.CurrentRow.Index].Value.ToString();
                txtNome.Text = dgvMedico[1, dgvMedico.CurrentRow.Index].Value.ToString();
                txtEspecialidade.Text = dgvMedico[2, dgvMedico.CurrentRow.Index].Value.ToString();
                txtEndereco.Text = dgvMedico[3, dgvMedico.CurrentRow.Index].Value.ToString();
                txtComplemento.Text = dgvMedico[4, dgvMedico.CurrentRow.Index].Value.ToString();
                maskTxtTelFixo.Text = dgvMedico[5, dgvMedico.CurrentRow.Index].Value.ToString();
                maskTxtCelular.Text = dgvMedico[6, dgvMedico.CurrentRow.Index].Value.ToString();
                txtEmail.Text = dgvMedico[7, dgvMedico.CurrentRow.Index].Value.ToString();
            
               
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco!";
            }
        }


        private void FormatarGrid()
        {
            try
            {
                dgvMedico.Columns[0].HeaderText = ("CRM");
                dgvMedico.Columns[0].Width = 130;

                dgvMedico.Columns[1].HeaderText = ("Nome");
                dgvMedico.Columns[1].Width = 210;

                dgvMedico.Columns[2].HeaderText = (" Especialidade ");
                dgvMedico.Columns[2].Width = 160;

                dgvMedico.Columns[3].HeaderText = ("Endereço");
                dgvMedico.Columns[3].Width = 210;

                dgvMedico.Columns[4].HeaderText = (" Complemento ");
                dgvMedico.Columns[4].Width = 180;

                dgvMedico.Columns[5].HeaderText = ("Telefone");
                dgvMedico.Columns[5].Width = 130;

                dgvMedico.Columns[6].HeaderText = ("celular");
                dgvMedico.Columns[6].Width = 130;

                dgvMedico.Columns[7].HeaderText = ("Email");
                dgvMedico.Columns[7].Width = 180;
            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco! " ;
            }
        }


        private void AtualizarGrid()
        {
            filtro = txtPesquisar.Text;
            try
            {
                dgvMedico.DataSource = objMedBll.Listagem(filtro);

            }
            catch
            {
                lblMensagem.Text = "Não foi possível conexão com o banco ";
            }
            finally
            {
                FormatarGrid();
            }


        }


        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtro = txtPesquisar.Text;

            try
            {
                dgvMedico.DataSource = objMedBll.Listagem(filtro);
                limpaControles(this);
            }
            catch
            {
                lblMensagem.Text = "Pesquisa não encontrada !";
            }

        }
         

        private void FormMedico_Load(object sender, EventArgs e)
        {
            try
            {
                if (Globais.ChamadaConsulta == true)
                {
                    btnAlterar.Visible = false;
                    btnIncluir.Visible = false;
                }
                 AtualizarGrid();
         
                // Estabelecendo e exibindo a data do sistema
                string strData, strDia, strMes, strAno;
                strDia = DateTime.Now.Day.ToString("00");
                strMes = DateTime.Now.Month.ToString("00");
                strAno = DateTime.Now.Year.ToString("0000");
                strData = strDia + "/" + strMes + "/" + strAno;
                lbldata.Text = strData;

                timer1.Enabled = true;

                timer1.Interval = 1000;

            }
            catch
            {
                lblMensagem.Text = "Não foi possível a conexão com o banco !";
            }
        }
   


        private void timer1_Tick(object sender, EventArgs e)
        {
            string strRelogio, strHora, strMinuto, strSegundo;
            strHora = DateTime.Now.Hour.ToString("00");
            strMinuto = DateTime.Now.Minute.ToString("00");
            strSegundo = DateTime.Now.Second.ToString("00");
            strRelogio = strHora + ":" + strMinuto + ":" + strSegundo;
            lblhora.Text = strRelogio;
        }


        private void limpaControles(Control controle)
        {
            lblMensagem.Text = "";
            foreach (Control ctrl in controle.Controls)
            {

                if (ctrl is TextBox)
                {                           
                    ((TextBox)ctrl).Text = string.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)ctrl).Text = string.Empty;
                }

                else if (ctrl.Controls.Count > 0)
                {
                    limpaControles(ctrl);
                }
            }

        }

      









    }


}
