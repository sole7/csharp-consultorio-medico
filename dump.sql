CREATE DATABASE  IF NOT EXISTS `clinica` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `clinica`;
-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: clinica
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consulta`
--

DROP TABLE IF EXISTS `consulta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consulta` (
  `idconsulta` int(11) NOT NULL AUTO_INCREMENT,
  `dataConsulta` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `cid` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `dataRetorno` date DEFAULT NULL,
  `dataAgendamento` date DEFAULT NULL,
  `exameSolicitado` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `diagnostico` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `CRMmedico` varchar(45) CHARACTER SET latin1 NOT NULL,
  `prontuario` int(11) NOT NULL,
  `idfuncionario` int(11) NOT NULL,
  PRIMARY KEY (`idconsulta`,`CRMmedico`,`prontuario`,`idfuncionario`),
  KEY `fk_consulta_paciente1_idx` (`prontuario`),
  KEY `fk_consulta_funcionario1_idx` (`idfuncionario`),
  KEY `fk_consulta_medico1_idx` (`CRMmedico`),
  CONSTRAINT `fk_consulta_funcionario1` FOREIGN KEY (`idfuncionario`) REFERENCES `funcionario` (`idfuncionario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_consulta_medico1` FOREIGN KEY (`CRMmedico`) REFERENCES `medico` (`CRM`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_consulta_paciente1` FOREIGN KEY (`prontuario`) REFERENCES `paciente` (`prontuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta`
--

LOCK TABLES `consulta` WRITE;
/*!40000 ALTER TABLE `consulta` DISABLE KEYS */;
INSERT INTO `consulta` VALUES (2,'2016-04-17','05:04:20',130,'3456','2016-04-30','2016-03-27','','','899876-yh',3,10),(3,'2016-04-12','05:04:20',100,'','2016-04-01','2016-04-01','','','899876-yh',6,11),(5,'2016-04-05','08:00:00',200,'','2016-04-05','2016-04-05','','','12345-lo',6,12),(6,'2016-06-01','10:00:00',150,'','2016-07-01','2016-04-05','','','899876-yh',10,12);
/*!40000 ALTER TABLE `consulta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convenio`
--

DROP TABLE IF EXISTS `convenio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convenio` (
  `idConvenio` int(11) NOT NULL AUTO_INCREMENT,
  `CNPJ` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `contato` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`idConvenio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convenio`
--

LOCK TABLES `convenio` WRITE;
/*!40000 ALTER TABLE `convenio` DISABLE KEYS */;
INSERT INTO `convenio` VALUES (1,'265,698,658/8569-95','Amil Assistênia Médica','Ana Mello da Costa','(11)4589-3625'),(2,'235,689,854/5698-74','Bradesco Saúde','Roberto Santos','(11)3265-9874'),(3,'235,689,854/7454-52','Unimed Paulistana','Fátima Barboza','(11)4895-6524'),(4,'123,564,879/5155-85','BioSaúde ','Júlio Moraes','(11)8965-7454');
/*!40000 ALTER TABLE `convenio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcionario` (
  `idfuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `CPF` varchar(14) CHARACTER SET latin1 DEFAULT NULL,
  `RG` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `endereco` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `complemento` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `telefoneFixo` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `celular` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`idfuncionario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionario`
--

LOCK TABLES `funcionario` WRITE;
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
INSERT INTO `funcionario` VALUES (10,'Luiz Carlos','87.987.987.98','77.657.987-5','R. Malia','casa 4','luiz@hotmail','4567-4568','9876-9865'),(11,'Caique Oliveira Burssed','294.569.458-35','66.895.365-8','R: Condominio Torres','casa 56','caique@yah','4521-9685','96589-9658'),(12,'Eloi da Silva','256,545,454/54','44,545,454/5','R: Pade Estanilsau de Campos,380','apt 42B Jardim Hercília','eloi@ig.com','(11)2565-4545','(11)58954-5545');
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medico` (
  `CRM` varchar(45) CHARACTER SET latin1 NOT NULL,
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `especialidade` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `endereco` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `complemento` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `celular` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`CRM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES ('12345-lo','Pedro Paulo','infectologista','R. Coronel Almeida,325',' Praça Leme -apt 31-a','(11)4524-5488','(11)99876-598','pedro@yahoo.com'),('89876-kij','Baltazar','cardiologista','R.Padre Estevaõ','apt 31-a','(11)8954-5525','(11)99876-598','8765-9865'),('899876-yh','Baltazar','cardiologista','R.Padre Estevaõ','apt 31-a','8765-9865','98765-98','baltazar@uol');
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `prontuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sexo` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CPF` varchar(14) CHARACTER SET latin1 DEFAULT NULL,
  `RG` varchar(14) CHARACTER SET latin1 DEFAULT NULL,
  `dtaNascimento` date DEFAULT NULL,
  `endereco` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `complemento` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `telefoneFixo` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `celular` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `idConvenio` int(11) DEFAULT NULL,
  PRIMARY KEY (`prontuario`),
  KEY `fk_convenio_idx` (`idConvenio`),
  CONSTRAINT `fk_convenio` FOREIGN KEY (`idConvenio`) REFERENCES `convenio` (`idConvenio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (2,'Pedro Barboza','M','236.569.658-58','33.658.658-7','2008-06-01','Av: Waldemar Carlos Pereira,380 ','apto 42-c  Jardim Iguatemi','(11)4562-8569','(11)98547-6532','pedro_barboza@ig.com.br',1),(3,'Caique Oliveira Burssed','M','249.458.658-95','33.587.369-4','2009-01-02','R: Palmerina De Souza,76','Vila Carrão','(11)4035-8565','(11)98587-9854','caique@yahoo.com.br',1),(4,'Carloina Germano','F','383,097,128-12','48,707,945-5','1998-07-03','R: Sete de Setembro, 562','Tatuapé','(11)7865-4444','(11)76543-222','carolina@hotmail.com',1),(5,'Silvio da Silva','M','256.658.362-21','55.698.652-7','1970-02-01','R: Maira de Paula, 547','Vila Bianchi','(11)5896-5263','(11)2563-8957','silvio@gmail.com',1),(6,'Berenice Almeida','F','236.658.587-87','55.325.987-5','1946-02-06','R: Waldemar Carlos Pereira, 896','Cidade Tiradentes','(11)5689-8584','(11)5968-2563','berenice@hotmail.com',1),(7,'André Matheu Nogueira','M','294,586,589-55','33,587,369-4','2009-01-02','R;Palmerina','casa 76 Jardim Eurovile','(11)4035-8565','(11)98587-9854','dématheus@yahoo',2),(8,'Abel Martins Pereira','F','383,097,128-12','48,707,945-0','1998-07-03','R: Eugênio Ripoli, 577','Vila Isabel','(11)7865-4444','(11)97654-3222','abel@ig.com.br',1),(9,'Fátima da Costa ','F','256,845,712-34','33,598,566-7','1970-02-01','R: Barão de Paranabiacapa, 1520','Praça da Sé','(11)4334-3434','(11)99556-4433','abreu@hotmail.com',1),(10,'Letícia de Castro','F','294,586,589-54','33,587,369-4','2009-01-02','R: Flores Azuis, 521',' Ibirapuera','(11)4035-8565','(11)98587-9854','letis@yahoo',1),(11,'João Carlos Moura','M','526,389,411-11','66,875,249-2','1957-01-01','R: Pedra Da Sorte, 632','Penha','(11)5625-8965','(11)93265-8725','joao_moura@yahooa',1),(12,'João Carlos Moura','M','236,589,874-54','45,896,215-4','1993-10-18','R;Iguatemi,76','Bragança III','(11)5897-8565','(11)98587-2568','joao_moura@yahoo',1),(13,'Letícia Nogueria','F','235,689,568-74','55,698,741-1','1973-03-01','R: Waldemar Carlos Pereira, 256','Jardim Iguatemi','(11)5698-1558','(11)98652-3658','letica@yahoo',1);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(7) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'ana','123456','administrador'),(2,'pedro','654321','administrador'),(3,'walter','123458','usuario'),(4,'eloi','123456','medico'),(7,'ralf','101010','usuario');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'clinica'
--

--
-- Dumping routines for database 'clinica'
--
/*!50003 DROP PROCEDURE IF EXISTS `AlterarConsulta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarConsulta`(in pidconsulta int ,in pdataConsulta date, in phora time, in ppreco float, in pcid varchar(10), in pdataRetorno date, in pdataAgendamento date, in pexameSolicitado varchar(60), in pdiagnostico varchar(60), in pCRMmedico varchar(45), in pprontuario int, in pidfuncionario int)
BEGIN
	update consulta set dataConsulta=pdataConsulta, hora=phora, preco=ppreco, cid=pcid, dataRetorno = pdataRetorno, dataAgendamento = pdataAgendamento, exameSolicitado=pexameSolicitado, diagnostico=pdiagnostico, CRMmedico=pCRMmedico, prontuario=pprontuario, idfuncionario=pidfuncionario 
     where idconsulta=pidconsulta;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AlterarConvenio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarConvenio`(in pidConvenio int, in pcnpj varchar(30), in pnome varchar(45), in pcontato varchar(45), in ptelefone varchar(20))
BEGIN
	update convenio set cnpj=pcnpj, nome=pnome, contato=pcontato, telefone=ptelefone
		where idConvenio=pidConvenio;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AlterarFuncionario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarFuncionario`(in pidfuncionario int, in pnome varchar(45),in pcpf varchar(14), in prg varchar(15), in pendereco varchar(70), in pcomplemento varchar(50), in pemail varchar(45), in ptelFixo varchar(20), in pcelular varchar(20) )
BEGIN
	update  funcionario set nome=pnome, cpf=pcpf, rg=prg, endereco=pendereco, complemento=pcomplemento, email=pemail, telefoneFixo=ptelFixo, celular=pcelular 
					where idfuncionario=pidfuncionario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AlterarMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarMedico`(in pcrm varchar(45),in pnome varchar(45),in pespecialidade varchar(45),  in pendereco varchar(70), in pcomplemento varchar(50), in pcelular varchar(20), in ptelefone varchar(20), in pemail varchar(45) )
BEGIN
	update medico set nome=pnome, especialidade=pespecialidade, endereco=pendereco, complemento=pcomplemento, email=pemail, telefone=ptelefone, celular=pcelular
		where crm=pcrm;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AlterarPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarPaciente`(in pprontuario int, in pnome varchar(45), in psexo char(1), in pcpf varchar(14), in prg varchar(14), in pdtaNascimento date, in pendereco varchar(70), in pcomplemento varchar(50), in ptelefoneFixo varchar(20), in pcelular varchar(20), in pemail varchar(30), in pidConvenio int)
BEGIN
	update paciente set nome=pnome, sexo=psexo, cpf=pcpf, rg=prg, dtaNascimento=pdtaNascimento, endereco=pendereco, complemento=pcomplemento, telefoneFixo=ptelefoneFixo, celular=pcelular, email=pemail, idConvenio = pidConvenio
		where pprontuario=prontuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AlterarUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AlterarUsuarios`(in psenha varchar(7), in ptipo varchar(15), in pidUsuario int, in pusuario varchar(45))
BEGIN
	update usuarios set senha=psenha, tipo= ptipo, usuario=pusuario
		where idusuario = pidusuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ExcluirConsulta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ExcluirConsulta`(in pidConsulta int)
BEGIN
	delete from consulta where idConsulta = pidConsulta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ExcluirUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ExcluirUsuario`(in pidusuario int)
BEGIN
	delete from usuarios where idusuario = pidusuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IncluirConvenio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncluirConvenio`(in pcnpj varchar(30), in pnome varchar(45), in pcontato varchar(45), in ptelefone varchar(20))
BEGIN
	insert into convenio values (null, pcnpj, pnome, pcontato, ptelefone);
    select idConvenio from convenio where idConvenio = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IncluirFuncionario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncluirFuncionario`(in pnome varchar(45),in pcpf varchar(14), in prg varchar(15), in pendereco varchar(70), in pcomplemento varchar(50),in pemail varchar(45), in ptelFixo varchar(20), in pcelular varchar(20))
BEGIN
	insert into funcionario values(null, pnome, pcpf, prg, pendereco, pcomplemento, pemail, ptelFixo, pcelular);
    select * from funcionario where idfuncionario = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IncluirMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncluirMedico`(in pcrm varchar(45),in pnome varchar(45),in pespecialidade varchar(45),  in pendereco varchar(70), in pcomplemento varchar(50), in pcelular varchar(20), in ptelefone varchar(20), in pemail varchar(45) )
BEGIN
	insert into medico values( pcrm,pnome, pespecialidade, pendereco, pcomplemento, pcelular, ptelefone, pemail);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IncluirPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncluirPaciente`(in pnome varchar(45), in psexo char(1), in pcpf varchar(14), in prg varchar(14), in pdtaNascimento date, in pendereco varchar(70), in pcomplemento varchar(50), in ptelefoneFixo varchar(20), in pcelular varchar(20), in pemail varchar(30), in pidConvenio int)
BEGIN
	insert into paciente values(null, pnome, psexo, pcpf, prg, pdtaNascimento, pendereco, pcomplemento, ptelefoneFixo, pcelular, pemail, pidConvenio);
    select prontuario from paciente where prontuario = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IncluirUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncluirUsuario`(in psenha varchar(7), in ptipo varchar(15), in pusuario varchar(45))
BEGIN
	insert into usuarios (senha, tipo, usuario) values (psenha, ptipo, pusuario);
    select idusuario from usuarios where idusuario = last_insert_id() ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarConsulta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarConsulta`(in pfiltro varchar(20))
BEGIN
	if(pfiltro = '')then
		select * from consulta  ;
     else 
		select idconsulta, date_format(dataConsulta,'%d/%m/%Y'), hora , preco, cid,
        date_Format(dataRetorno,'%d/%m/%Y'),date_format(dataAgendamento,'%d/%m/%Y'),
        exameSolicitado, diagnostico, CRMmedico, prontuario, idfuncionario 
            from consulta where date_format(dataConsulta,'%d/%m/%Y') like concat('%',pfiltro,'%')
            or prontuario like concat('%',pfiltro,'%') ;
 
end if; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarConvenio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarConvenio`( in pfiltro varchar(40))
BEGIN
	if(pfiltro = '' )then
		select * from convenio order by nome;
     else 
		select * from convenio where nome like concat('%',pfiltro,'%') or
			idConvenio like concat('%',pfiltro,'%') order by nome;
            
end if;            
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarFuncionario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarFuncionario`(in pfiltro varchar(20))
BEGIN
	if(pfiltro = null or pfiltro='')then
    	select * from funcionario order by nome;
    else
        select * from funcionario 
        where nome like concat('%',pfiltro,'%')
        or    idfuncionario like concat('%',pfiltro,'%') order by nome;
        
     end if;        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarMedico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarMedico`(in pfiltro varchar(20))
BEGIN
	if(pfiltro = " ")then
		select * from medico order by nome;
    else
		select * from medico 
			where nome like concat('%',pfiltro,'%')
             or CRM like concat('%',pfiltro,'%')
               or especialidade like concat('%',pfiltro,'%') order by nome;
               
End if;            
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarPaciente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarPaciente`( in pfiltro varchar(20))
BEGIN
	if(pfiltro = '')then
		select * from paciente order by nome;
    else
		select * from paciente 
			where nome like concat('%',pfiltro,'%')or
				prontuario like concat('%',pfiltro,'%') order by nome;
end if;           
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuario`(in pfiltro varchar(15))
BEGIN
	if(pfiltro = '')then
		select * from usuarios order by usuario;
    else 
		select * from usuarios where usuario like concat('%',pfiltro,'%')or
			idusuario like concat('%',pfiltro,'%');
end if;            
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ListarUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuarios`(in pfiltro varchar(15))
BEGIN
	if(pfiltro = '')then
		select * from usuarios order by usuario;
    else 
		select * from usuarios where usuario like concat('%',pfiltro,'%')or
			idusuario like concat('%',pfiltro,'%');
end if;            
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Login`(in pusuario varchar(45), in psenha varchar(7), in ptipo varchar(15))
BEGIN
	select * from usuarios where usuario=pusuario and senha = psenha and tipo = ptipo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MarcarConsulta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `MarcarConsulta`(in pdataConsulta date, in phora time, in ppreco float, in pcid varchar(10), in pdataRetorno date, in pdataAgendamento date, in pexameSolicitado varchar(60), in pdiagnostico varchar(60), in pCRMmedico varchar(45), in pprontuario int, in pidfuncionario int )
BEGIN
	insert into consulta values (null, pdataConsulta, phora, ppreco, pcid, pdataRetorno, pdataAgendamento, pexameSolicitado, pdiagnostico, pCRMmedico, pprontuario, pidfuncionario);
    select idconsulta from consulta where idconsulta = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-28  1:35:44
