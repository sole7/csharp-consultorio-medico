﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaMedica.Modelo
{
    public class Consulta

    {
        private int idFuncionario;
        public int IdFuncionario
        {
            get { return idFuncionario; }
            set { idFuncionario = value; }
        }


        private DateTime dataAgendamento;
        public DateTime DataAgendamento
        {
            get { return dataAgendamento; }
            set { dataAgendamento = value; }
        }

        private int prontuario;
        public int Prontuario
        {
            get { return prontuario; }
            set { prontuario = value; }
        }

        private int idConsulta;
        public int IdConsulta
        {
            get { return idConsulta ; }
            set { idConsulta = value; }
        }

        private string crmMedico;
        public string CrmMedico
        {
            get { return crmMedico; }
            set { crmMedico = value; }
        }

        private DateTime dataConsulta;
        public DateTime DataConsulta
        {
            get { return dataConsulta; }
            set { dataConsulta = value; }
        }

        private string hora;
        public string Hora
        {
            get { return hora; }
            set { hora = value; }
        }

        private float preco;
        public float Preco
        {
            get { return preco ; }
            set { preco = value; }
        }


        private string diagnostico;
        public string Diagnostico
        {
            get { return diagnostico; }
            set { diagnostico = value; }
        }

        private string cid;
        public string Cid
        {
            get { return cid; }
            set { cid = value; }
        }


        private string exame;
        public string Exame
        {
            get { return exame; }
            set { exame = value; }
        }


        private DateTime dataRetorno;
        public DateTime DataRetorno
        {
            get { return dataRetorno; }
            set { dataRetorno = value; }
        }

       

    }
}
