﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaMedica.Modelo
{
    public class Funcionario
    {
        private int idFuncionario;
        public int IdFuncionario
        {
            get { return idFuncionario; }
            set { idFuncionario = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string cpf;
        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }

        private string rg;
        public string Rg
        {
            get { return rg; }
            set { rg = value; }
        }

        private string endereco;
        public string Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }

        private string complemento;
        public string Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string telFixo;
        public string TelFixo
        {
            get { return telFixo; }
            set { telFixo = value; }
        }


        private string celular;
        public string Celular
        {
            get { return celular; }
            set { celular = value; }
        }


      
    }
}
