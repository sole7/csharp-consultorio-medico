﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaMedica.Modelo
{
     public class Paciente
    {
        private string prontuario;
        public string Prontuario
        {
            get { return prontuario; }
            set { prontuario = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string sexo;
        public string Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        private string cpf ;
        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }


        private string rg;
        public string Rg
        {
            get { return rg; }
            set { rg = value; }
        }
        private DateTime  dataNascimento;
        public DateTime DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        private string endereco;
        public string Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }
        
        private string complemento;
        public string Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }

        private string telFixo;
        public string TelFixo
        {
            get { return telFixo; }
            set { telFixo = value; }
        }

        private string celular;
        public string Celular
        {
            get { return celular; }
            set { celular = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private int convenio;
        public int Convenio
        {
            get { return convenio; }
            set { convenio = value; }
        }


    }
}
