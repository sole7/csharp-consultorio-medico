﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaMedica.Modelo
{
    public class Medico
    {
        private string crm;
        public string Crm
        {
            get { return crm; }
            set { crm = value; }
        }
        
        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string especialilidade;
        public string Especialidade
        {
            get { return especialilidade; }
            set { especialilidade = value; }
        }


        private string endereco;
        public string Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }

        private string complemento;
        public string Complemento
        {
            get { return complemento ; }
            set { complemento = value; }
        }
        private string telefone;
        public string Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }
        private string celular;
        public string Celular
        {
            get { return celular; }
            set { celular = value; }
        }
        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
       
    }
}
