﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaMedica.Modelo
{
    public class Convenio
    {
        private int idConvenio;
        public int IdConvenio
        {
            get { return idConvenio; }
            set { idConvenio = value; }
        }

        private string cnpj;
        public string Cnpj
        {
            get { return cnpj; }
            set { cnpj = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string contato;
        public string Contato
        {
            get { return contato; }
            set { contato = value; }
        }

        private string telefone;
        public string Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }

       
    }
}
