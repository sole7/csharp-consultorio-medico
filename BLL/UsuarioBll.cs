﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using ClinicaMedica.DAL;

namespace ClinicaMedica.BLL
{
    public class UsuarioBll
    {
        UsuarioDal objUsuarioDal = new UsuarioDal();

        string mensagem;
        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        public bool Login(Usuario usuario)
        {
             try
            {
                if (usuario.Senha.Length < 6 || usuario.Senha.Length > 6)
                {
                    Mensagem = "Por favor, digite até 06 letras ou números para a senha!";
                   return false;
                }
                else if (usuario.Nome.Length <= 0)
                {
                    Mensagem = "Por favor, digite o Login! ";
                  return false;
                }
                else if (usuario.Tipo.Length <= 0)
                {
                    Mensagem = "Por favor, escolha um tipo para o usuário!";
                   return false;
                }
                else
                {
                  return  objUsuarioDal.Login(usuario);
                   
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }





        public void IncluirUsuario(Usuario usuario)
        {
            try
            {

                if (usuario.Senha.Length < 6 || usuario.Senha.Length > 6 )
                {
                    Mensagem = "Por favor, digite até 06 letras ou números para a senha!";
                }
                else if (usuario.Nome.Length <= 0)
                {
                    Mensagem = "Por favor, digite o nome de usuário! ";
                }
                else if (usuario.Tipo.Length <= 0)
                {
                    Mensagem = "Por favor, escolha um tipo para o usuário!";
                }
                else
                {
                    objUsuarioDal.IncluirUsuario(usuario);
                    Mensagem = "Usuário incluído com sucesso!";
                }
            }
            catch
            {
                Mensagem = "Desculpe, cadastro não realizado !";
            
            }

        }

        public void AlterarUsuario(Usuario usuario)
        {
            try
            {
                if (usuario.Id <= 0)
                {
                    this.mensagem = "Desculpe, usuário inválido!";
                }
                else if (usuario.Senha.Length < 6 || usuario.Senha.Length > 6)
                {
                    this.mensagem = "Por favor, digite até 06 letras ou números para a senha!";
                }
                else if (usuario.Nome.Length <= 0)
                {
                    this.mensagem = "Por favor, digite o nome de usuário! ";
                }
                else if (usuario.Tipo.Length <= 0)
                {
                    this.mensagem = "Por favor, escolha um tipo para o usuário!";
                }
                else
                {
                     objUsuarioDal.AlterarUsuario(usuario);
                     this.mensagem = " Alteração realizada com sucesso! ";
                }

            }
            catch
            {
                this.mensagem = "Desculpe, alterações não realizadas !";
            }


        }
        public void  ExcluirUsuario(Usuario usuario)
        {
            try
            {

                if (usuario.Id <= 0)
                {
                    Mensagem = "Por favor, selecione um usuário para a exclusão!  ";

                }
                else
                {
                    objUsuarioDal.ExcluirUsuario(usuario);

                    Mensagem = " Usuário excluido com sucesso! ";
                }
            }
            catch
            {
                Mensagem = "Desculpe, não foi possível excluir o usuário desejado! ";
            }

        }

         public ListUsuario Listagem(string filtro)
        {
            try
            {
                return objUsuarioDal.Listagem(filtro);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
          
            
        }
    }
}
