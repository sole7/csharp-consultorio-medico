﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.DAL;
using ClinicaMedica.Modelo;


namespace ClinicaMedica.BLL
{
    public class FuncionarioBll
   {

        FuncionarioDal objDall = new FuncionarioDal();
       
        private string mensagem;

        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }


        public string IncluirFuncionario(Funcionario objFunc)
        {        
            try
            {
                string testeCPF = objFunc.Cpf.Replace(" ", "").Replace(",", "").Replace("-","");
                string testeCelular = objFunc.Celular.Replace(" ", "").Replace("(", "").Replace("-","").Replace(")","");

                if ((objFunc.Nome.Length <= 3) ||  (testeCPF.Length <= 0) || (testeCelular.Length <= 0))
                {
                   return Mensagem = " Campos: Nome, CPF e Celular são obrigatórios";

                }
                else
                {
                    objDall.IncluirFuncionario(objFunc);
                    return Mensagem = " Funcionario cadastrado com sucesso !";
                }
  
            }
            catch 
            {
                return Mensagem = "Funcionario não pode ser cadastrado !";
              
            }
        }


        public string AlterarFuncionario(Funcionario objFunc)
        {
            try
            {
                string testeCPF = objFunc.Cpf.Replace(" ", "").Replace(",", "").Replace("-", "");
                string testeCelular = objFunc.Celular.Replace(" ", "").Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objFunc.Nome == "") || (testeCelular.Length <= 0) || (testeCPF.Length <= 0))
                {
                    return Mensagem = " Campos Nome, CPF e Celular são obrigatórios";
                }
                else
                {
                    objDall.AlterarFuncionario(objFunc);
                    return Mensagem = " Alterações foram realizadas com sucesso !";
                }

            }
            catch
            {
                return Mensagem = " Não foi possível a alteração dos dados do funcionário!";
            }
        }

        public ListarFuncionario Listagem(string filtro)
        {
            try
            {
               
                      return objDall.Listagem(filtro);
            }    
            catch
            {
                throw new Exception(" Não foi possível listar funcionários");
            }
        }
     

    }
}
