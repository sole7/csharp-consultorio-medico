﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.DAL;
using ClinicaMedica.Modelo;


namespace ClinicaMedica.BLL
{
    public class ConsultaBll
    {
        private string mensagem;

        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        ConsultaDal objConsulDal = new ConsultaDal();

        public string MarcarConsulta(Consulta objConsul)
        {
            try
            {
                if ((objConsul.IdFuncionario < 0 ) || (objConsul.Prontuario < 0))
                {
                    return Mensagem = " Campos Código Funcionário e Prontuário são obrigatórios";

                }
                else
                {
                    objConsulDal.MarcarConsulta(objConsul);
                    return Mensagem = " Consulta cadastrada com sucesso !";
                }

            }
            catch
            {
                return Mensagem = "Não foi possível cadastrar a Consulta ! ";
            }
        }
            
        public string AlterarConsulta(Consulta objConsul)
        {
            try
            {
                if ((objConsul.IdFuncionario < 0) || (objConsul.Prontuario < 0) )
                {
                    return Mensagem = " Campos Código Funcionário e Prontuário são obrigatórios";

                }
                else
                {
                    objConsulDal.AlterarConsulta(objConsul);
                    return Mensagem = " Consulta alterada com sucesso !";
                }

            }
            catch
            {
                return Mensagem = "Não foi possível alterar a Consulta ! ";
            }

        }


        public string ExcluirConsulta(Consulta objConsul)
        {

            try
            {
                if (objConsul.IdConsulta < 1 )
                {
                    return Mensagem = "Por favor, escolha o ID da consulta para exclusão!";
                }
                else
                {
                    objConsulDal.ExcluirConsulta(objConsul);
                    return Mensagem = "Consulta excluída com sucesso!  ";
                }


            }
            catch (Exception ex)
            {
                return Mensagem = "Não foi possível exculir a Consulta ! ";
            }

        }

        public ListConsulta Listagem(string filtro)
        {
            try
            {
               return objConsulDal.Listagem(filtro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        
        
    }
}
