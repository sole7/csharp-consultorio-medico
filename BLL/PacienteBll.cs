﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using ClinicaMedica.DAL;

namespace ClinicaMedica.BLL
{
    public class PacienteBll
    {
        private string mensagem;
        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        PacienteDall objPacDall = new PacienteDall();


        public string IncluirPaciente(Paciente objPac)
        {
            try
            {
                string testeCelular = objPac.Celular.Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objPac.Nome.Length<=0) || (testeCelular.Length <=0) || (objPac.Cpf.Length<=0))
                {
                    return Mensagem = "Campos: Nome e Celular são obrigatórios!";

                }
                else
                {
                    objPacDall.IncluirPaciente(objPac);
                    return Mensagem = "Paciente Incluído com sucesso !";
                }

            }
            catch
            {
                throw new Exception("Paciente não pode ser cadastrado !");

            }

        }

        public string AlterarPaciente(Paciente objPac)
        {
            try
            {
                string testeCelular = objPac.Celular.Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objPac.Nome == "") || (testeCelular.Length <= 0))
                {
                    return Mensagem = " Campos: Nome e Celular são obrigatórios";

                }
                else
                {
                    objPacDall.AlterarPaciente(objPac);
                    return mensagem = " Alteração realizada com sucesso ! ";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        public ListPaciente Listagem(string filtro)
        {
            try
            {
               return  objPacDall.Listagem(filtro);
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
