﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using ClinicaMedica.DAL;

namespace ClinicaMedica.BLL
{
    public class ConvenioBll
    {
        private string mensagem;

        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        ConvenioDall objConvDal = new ConvenioDall();

        
        public string IncluirConvenio(Convenio objConv)
        {        
            try
            {
                string testeCNPJ = objConv.Cnpj.Replace(" ", "").Replace(",", "").Replace("-", "");
                string testeTelefone = objConv.Contato.Replace(" ", "").Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objConv.Nome == "") ||  (testeCNPJ.Length <= 0) || (testeTelefone.Length <= 0))
                {
                   return Mensagem = " Todos os campos são obrigatórios para inclusão!";

                }
                else
                {
                    objConvDal.IncluirConvenio(objConv);
                    return Mensagem = " Convênio cadastrado com sucesso !";
                }
  
            }
            catch 
            {
                return Mensagem = "Não foi possível cadastrar o convênio ";
            }
        }


        public string AlterarConvenio(Convenio objConv)
        {
            try
            {
                string testeCNPJ = objConv.Cnpj.Replace(" ", "").Replace(",", "").Replace("-", "");
                string testeTelefone = objConv.Contato.Replace(" ", "").Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objConv.Nome == "") || (testeCNPJ.Length <= 0) || (objConv.Contato == "") || (testeTelefone.Length <= 0))
                {
                    return Mensagem = " Todos os campos são obrigatórios para alteração!";
                }

                else
                {
                    objConvDal.AlterarConvenio(objConv);
                    return Mensagem = " Alterações realizadas com sucesso !";
                }

            }
            catch
            {
                 return Mensagem = " Não foi possível a alteração!";
            }
            
        }

        public ListConvenio Listagem(string filtro)
        {
            try
            {
                return objConvDal.Listagem(filtro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
                  
        }
    }
}
