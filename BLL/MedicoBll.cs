﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.DAL;
using ClinicaMedica.Modelo;

namespace ClinicaMedica.BLL
{
    public class MedicoBll
    {
        MedicoDall objMedDall = new MedicoDall();
        
        private string mensagem;
        public string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        public string IncluirMedico(Medico objMed)
        {
            try
            {
                String testeCelular = objMed.Celular.Replace (" ", "").Replace("(", "").Replace("-","").Replace(")","");

                 if ((objMed.Nome == "") || (testeCelular.Length <= 0) || (objMed.Celular == "")||(objMed.Especialidade == ""))
                {
                   return Mensagem = "Campos: Nome, CRM , Celular e Especialidade são obrigatórios!";

                }
                else
                {
                    objMedDall.IncluirMedico(objMed);
                    return Mensagem = " Médico cadastrado com sucesso !";
                }
  
            }
            catch 
            {
                return Mensagem = "Medico não pode ser cadastrado !";
                
            }
           
        }

        public string AlterarMedico(Medico objMed)
        {
            try
            {
                String testeCelular = objMed.Celular.Replace(" ", "").Replace("(", "").Replace("-", "").Replace(")", "");

                if ((objMed.Nome == "") ||  (objMed.Crm == "") || (testeCelular.Length <= 0) || (objMed.Especialidade == ""))
                {
                    return Mensagem = "Campos: Nome, CRM , Celular e Especialidade são obrigatórios";

                }
                else
                {
                    objMedDall.AlterarMedico(objMed);
                    return Mensagem = " Alteração realizada com sucesso ! ";
                }
            }
            catch 
            {
                return Mensagem = "Não foi possível a alteração nos dados do médico !";
            }
        }

        public ListMedico Listagem(string filtro)
        {
            try
            {
                return objMedDall.Listagem(filtro);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
