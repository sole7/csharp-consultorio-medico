﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;
using ClinicaMedica.Modelo;


namespace ClinicaMedica.DAL
{

    public class ConsultaDal
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection con = new MySqlConnection();


        public void MarcarConsulta(Consulta objConsul)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "MarcarConsulta";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pdataConsulta", objConsul.DataConsulta);
                cmd.Parameters.AddWithValue("phora", objConsul.Hora);
                cmd.Parameters.AddWithValue("ppreco", objConsul.Preco);
                cmd.Parameters.AddWithValue("pcid", objConsul.Cid);
                cmd.Parameters.AddWithValue("pdataRetorno", objConsul.DataRetorno);
                cmd.Parameters.AddWithValue("pdataAgendamento", objConsul.DataAgendamento);
                cmd.Parameters.AddWithValue("pexameSolicitado", objConsul.Exame);
                cmd.Parameters.AddWithValue("pdiagnostico", objConsul.Diagnostico);
                cmd.Parameters.AddWithValue("pCRMmedico", objConsul.CrmMedico);
                cmd.Parameters.AddWithValue("pprontuario", objConsul.Prontuario);
                cmd.Parameters.AddWithValue("pidfuncionario", objConsul.IdFuncionario);

                con.Open();

                objConsul.IdConsulta = Convert.ToInt32(cmd.ExecuteScalar().ToString());
          
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public void AlterarConsulta(Consulta objConsul)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "AlterarConsulta";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pidConsulta", objConsul.IdConsulta);
                cmd.Parameters.AddWithValue("pdataConsulta", objConsul.DataConsulta);
                cmd.Parameters.AddWithValue("phora", objConsul.Hora);
                cmd.Parameters.AddWithValue("ppreco", objConsul.Preco);
                cmd.Parameters.AddWithValue("pcid", objConsul.Cid);
                cmd.Parameters.AddWithValue("pdataRetorno", objConsul.DataRetorno);
                cmd.Parameters.AddWithValue("pdataAgendamento", objConsul.DataAgendamento);
                cmd.Parameters.AddWithValue("pexameSolicitado", objConsul.Exame);
                cmd.Parameters.AddWithValue("pdiagnostico", objConsul.Diagnostico);
                cmd.Parameters.AddWithValue("pCRMmedico", objConsul.CrmMedico);
                cmd.Parameters.AddWithValue("pprontuario", objConsul.Prontuario);
                cmd.Parameters.AddWithValue("pidfuncionario", objConsul.IdFuncionario);

                con.Open();

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }


        public void ExcluirConsulta(Consulta objConsulta)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "ExcluirConsulta";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pidConsulta", objConsulta.IdConsulta);
                con.Open();
                cmd.ExecuteNonQuery();
            }

            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        public  Modelo.ListConsulta Listagem(string filtro) 
        {
            ListConsulta objlist = new ListConsulta();
            try
            {

                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "ListarConsulta";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pfiltro",filtro);

                con.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                if( dr.HasRows == true )
                {
                    while(dr.Read())
                    {
                        Consulta objconsul = new Consulta();

                        objconsul.IdConsulta = Convert.ToInt32(dr["idconsulta"].ToString());
                        objconsul.DataConsulta =Convert.ToDateTime(dr[1].ToString());
                        objconsul.Hora = dr["hora"].ToString();
                        objconsul.Preco = float.Parse(dr["preco"].ToString());
                        objconsul.Cid = dr["cid"].ToString();
                        objconsul.DataRetorno =  Convert.ToDateTime(dr[5].ToString());
                        objconsul.DataAgendamento = Convert.ToDateTime(dr[6].ToString());
                        objconsul.Exame = dr["exameSolicitado"].ToString();
                        objconsul.Diagnostico = dr["diagnostico"].ToString();
                        objconsul.CrmMedico = dr["CRMmedico"].ToString();
                        objconsul.Prontuario = Convert.ToInt32(dr["prontuario"].ToString());
                        objconsul.IdFuncionario =  Convert.ToInt32(dr["idfuncionario"].ToString());

                        objlist.Add(objconsul);
                        
                        
                    }

                }

                return objlist;

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
    }








    }
 }
    

