﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ClinicaMedica.DAL
{
    public class ConvenioDall
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection con = new MySqlConnection();

        public void IncluirConvenio(Convenio objConvenio)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "IncluirConvenio";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pcnpj", objConvenio.Cnpj);
                cmd.Parameters.AddWithValue("pnome", objConvenio.Nome);
                cmd.Parameters.AddWithValue("pcontato", objConvenio.Contato);
                cmd.Parameters.AddWithValue("ptelefone", objConvenio.Telefone);
             
                con.Open();

                objConvenio.IdConvenio = int.Parse(cmd.ExecuteScalar().ToString());

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public void AlterarConvenio(Convenio objConvenio)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "AlterarConvenio";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pidConvenio", objConvenio.IdConvenio);
                cmd.Parameters.AddWithValue("pcnpj", objConvenio.Cnpj);
                cmd.Parameters.AddWithValue("pnome", objConvenio.Nome);
                cmd.Parameters.AddWithValue("pcontato", objConvenio.Contato);
                cmd.Parameters.AddWithValue("ptelefone", objConvenio.Telefone);
           
                con.Open();

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public Modelo.ListConvenio Listagem(string filtro)
        {
            try
            {
                ListConvenio  objList = new ListConvenio();

                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "ListarConvenio";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pfiltro",filtro);

                con.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                if(dr.HasRows == true)
                {
                    while(dr.Read())
                    {
                        Convenio objCon = new Convenio();

                        objCon.IdConvenio =  Convert.ToInt32(dr["idConvenio"]);
                        objCon.Cnpj = dr["cnpj"].ToString();
                        objCon.Nome = dr["nome"].ToString();
                        objCon.Contato = dr["contato"].ToString();
                        objCon.Telefone = dr["telefone"].ToString();
                   
                        objList.Add(objCon);

                    }
                  
                }
                return objList;
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

    }
}
