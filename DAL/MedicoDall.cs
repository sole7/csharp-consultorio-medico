﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using MySql.Data;
using MySql.Data.MySqlClient;



namespace ClinicaMedica.DAL
{
    public class MedicoDall
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection con = new MySqlConnection();

        public void IncluirMedico(Modelo.Medico objMed )
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "IncluirMedico";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pcrm", objMed.Crm);
                cmd.Parameters.AddWithValue("pnome",objMed.Nome);
                cmd.Parameters.AddWithValue("pespecialidade", objMed.Especialidade);
                cmd.Parameters.AddWithValue("pendereco", objMed.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento", objMed.Complemento);
                cmd.Parameters.AddWithValue("pemail", objMed.Email);
                cmd.Parameters.AddWithValue("pcelular", objMed.Celular);
                cmd.Parameters.AddWithValue("ptelefone", objMed.Telefone);
               
                con.Open();

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public void AlterarMedico(Medico objMed)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "AlterarMedico";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pcrm",objMed.Crm);
                cmd.Parameters.AddWithValue("pnome", objMed.Nome);
                cmd.Parameters.AddWithValue("pespecialidade", objMed.Especialidade);
                cmd.Parameters.AddWithValue("pendereco", objMed.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento", objMed.Complemento);
                cmd.Parameters.AddWithValue("pemail", objMed.Email);
                cmd.Parameters.AddWithValue("pcelular", objMed.Celular);
                cmd.Parameters.AddWithValue("ptelefone", objMed.Telefone);
               
                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch(MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }


        public Modelo.ListMedico Listagem( string filtro)
        {
            ListMedico objlist = new ListMedico();
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "ListarMedico";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pfiltro",filtro);

                con.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                if(dr.HasRows == true)
                {
                    while(dr.Read())
                    {
                        Medico objMed = new Medico();

                        objMed.Crm = dr["CRM"].ToString();
                        objMed.Nome = dr["nome"].ToString();
                        objMed.Especialidade = dr["especialidade"].ToString();
                        objMed.Endereco = dr["Endereco"].ToString();
                        objMed.Complemento = dr["Complemento"].ToString();
                        objMed.Telefone = dr["telefone"].ToString();
                        objMed.Celular = dr["celular"].ToString();
                        objMed.Email = dr["email"].ToString();

                        objlist.Add(objMed);

                    }

                }

                return objlist;
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }







    }
}
