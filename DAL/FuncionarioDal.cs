﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;
using ClinicaMedica.Modelo;

namespace ClinicaMedica.DAL
{
    public class FuncionarioDal
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection con = new MySqlConnection();
        
        public void IncluirFuncionario(Funcionario objfun)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;

                cmd.Connection = con;

                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.CommandText = "IncluirFuncionario";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pnome", objfun.Nome);
                cmd.Parameters.AddWithValue("pcpf", objfun.Cpf);
                cmd.Parameters.AddWithValue("prg", objfun.Rg);
                cmd.Parameters.AddWithValue("pendereco", objfun.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento",objfun.Complemento);
                cmd.Parameters.AddWithValue("pemail", objfun.Email);
                cmd.Parameters.AddWithValue("ptelfixo",objfun.TelFixo);
                cmd.Parameters.AddWithValue("pcelular",objfun.Celular);
              
                con.Open();

                objfun.IdFuncionario = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch(MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
      }


        public void AlterarFuncionario(Funcionario objfun)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;

                cmd.Connection = con;

                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.CommandText = "AlterarFuncionario";

              //  cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("pidfuncionario",objfun.IdFuncionario);
                cmd.Parameters.AddWithValue("pnome", objfun.Nome);
                cmd.Parameters.AddWithValue("pcpf", objfun.Cpf);
                cmd.Parameters.AddWithValue("prg", objfun.Rg);
                cmd.Parameters.AddWithValue("pendereco", objfun.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento", objfun.Complemento);
                cmd.Parameters.AddWithValue("pemail", objfun.Email);
                cmd.Parameters.AddWithValue("ptelfixo",objfun.TelFixo);
                cmd.Parameters.AddWithValue("pcelular",objfun.Celular);
             
                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        
                public Modelo.ListarFuncionario Listagem(string filtro)
                {
                    try
                    {
                        Modelo.ListarFuncionario objListar = new ListarFuncionario(); 

                        con.ConnectionString = Dados.strconexao;
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ListarFuncionario";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("pfiltro", filtro);
                        con.Open();

                        MySqlDataReader dr = cmd.ExecuteReader();

                        if(dr.HasRows==true)
                        {
                            while (dr.Read())
                            {
                                Funcionario objfun = new Funcionario();

                                objfun.IdFuncionario = int.Parse(dr["idfuncionario"].ToString());
                                objfun.Nome = dr["nome"].ToString();
                                objfun.Cpf = dr["cpf"].ToString();
                                objfun.Rg = dr["rg"].ToString();
                                objfun.Endereco = dr["endereco"].ToString();
                                objfun.Complemento = dr["complemento"].ToString();
                                objfun.Email = dr["email"].ToString();
                                objfun.TelFixo = dr["telefonefixo"].ToString();
                                objfun.Celular = dr["celular"].ToString();
                     
                                objListar.Add(objfun);

                            }
                    
                        }
                        return objListar;
                
                    }

                    catch (MySqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        con.Close();
                    }
                } 

    }
}
