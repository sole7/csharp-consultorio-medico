﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaMedica.Modelo;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ClinicaMedica.DAL
{
      public  class UsuarioDal
    {
          MySqlCommand cmd = new MySqlCommand();
          MySqlConnection con = new MySqlConnection();
          
          public bool Login(Usuario objUsuario)
          {
              bool validaUsuario=false, validaSenha=false, validaTipo=false , resposta=false;
               
              try
              {
                  con.ConnectionString = Dados.strconexao;
                  cmd.Connection = con;
                  cmd.CommandType = System.Data.CommandType.StoredProcedure;
                  cmd.CommandText = "Login";
                  cmd.Parameters.Clear();
                  cmd.Parameters.AddWithValue("psenha", objUsuario.Senha);
                  cmd.Parameters.AddWithValue("ptipo", objUsuario.Tipo);
                  cmd.Parameters.AddWithValue("pusuario", objUsuario.Nome);

                  con.Open();

                  MySqlDataReader dr = cmd.ExecuteReader();

                  if (dr.HasRows == true)
                  {
                      while (dr.Read())
                      {                  
                         if (dr["usuario"].ToString().ToUpper() == objUsuario.Nome.ToUpper()) validaUsuario = true ;
                         if ( dr["senha"].ToString() ==  objUsuario.Senha) validaSenha = true;
                         if (dr["tipo"].ToString().ToUpper() ==  objUsuario.Tipo.ToUpper()) validaTipo = true;
                      
                      }

                  }
                                 
                  if (validaUsuario == true && validaSenha == true && validaTipo == true)
                  {
                      resposta = true;
                      
                  }

                  return resposta;
              }
              catch (MySqlException ex)
              {
               
                  throw new Exception(ex.Message);
              }
              finally
              {
                    con.Close();
              }
          }

          
          public void IncluirUsuario(Modelo.Usuario objUsuario)
          {
              try
              {
                  con.ConnectionString = Dados.strconexao;
                  cmd.Connection = con;
                  cmd.CommandType = System.Data.CommandType.StoredProcedure;
                  cmd.CommandText = "IncluirUsuario";
                  cmd.Parameters.Clear();
                  cmd.Parameters.AddWithValue("psenha", objUsuario.Senha);
                  cmd.Parameters.AddWithValue("ptipo", objUsuario.Tipo);
                  cmd.Parameters.AddWithValue("pusuario", objUsuario.Nome);
                 
                  con.Open();

                  objUsuario.Id = Convert.ToInt32(cmd.ExecuteScalar());

              }
              catch (MySqlException ex)
              {
                  throw new Exception(ex.Message);
              }
              finally
              {
                  con.Close();
              }
          }

          public void AlterarUsuario(Usuario objUsuario)
          {
              try
              {
                  con.ConnectionString = Dados.strconexao;
                  cmd.Connection = con;
                  cmd.CommandType = System.Data.CommandType.StoredProcedure;
                  cmd.CommandText = "AlterarUsuarios";
                  cmd.Parameters.Clear();
                  cmd.Parameters.AddWithValue("pidUsuario", objUsuario.Id);
                  cmd.Parameters.AddWithValue("psenha", objUsuario.Senha);
                  cmd.Parameters.AddWithValue("ptipo", objUsuario.Tipo);
                  cmd.Parameters.AddWithValue("pusuario", objUsuario.Nome);

                  con.Open();

                  cmd.ExecuteNonQuery();
              }
              catch (MySqlException ex)
              {
                  throw new Exception(ex.Message);
              }
              finally
              {
                  con.Close();
              }
          }

          public void ExcluirUsuario(Usuario objUsuario)
          {
              try
              {
                  con.ConnectionString = Dados.strconexao;
                  cmd.Connection = con;
                  cmd.CommandType = System.Data.CommandType.StoredProcedure;
                  cmd.CommandText = "ExcluirUsuario";
                  cmd.Parameters.Clear();
                  cmd.Parameters.AddWithValue("pidUsuario", objUsuario.Id);
                 
                  con.Open();

                  cmd.ExecuteNonQuery();

              }
              catch (MySqlException ex)
              {
                  throw new Exception(ex.Message);
              }
              finally
              {
                  con.Close();
              }
          }

          public Modelo.ListUsuario Listagem(string filtro)
          {
              ListUsuario objlist = new ListUsuario();
              try
              {
                  con.ConnectionString = Dados.strconexao;
                  cmd.Connection = con;
                  cmd.CommandType = System.Data.CommandType.StoredProcedure;
                  cmd.CommandText = "ListarUsuario";
                  cmd.Parameters.Clear();
                  cmd.Parameters.AddWithValue("pfiltro", filtro);

                  con.Open();

                  MySqlDataReader dr = cmd.ExecuteReader();

                  if (dr.HasRows == true)
                  {
                      while (dr.Read())
                      {
                          Usuario objUsuario = new Usuario();

                          objUsuario.Id = Convert.ToInt32(dr["idUsuario"]);
                          objUsuario.Senha = dr["senha"].ToString();
                          objUsuario.Tipo = dr["tipo"].ToString();
                          objUsuario.Nome = dr["usuario"].ToString();
                          
                          objlist.Add(objUsuario);

                      }

                  }

                  return objlist;
              }
              catch (MySqlException ex)
              {
                  throw new Exception(ex.Message);
              }
              finally
              {
                  con.Close();
              }
          }



         
    }
}
