﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;
using ClinicaMedica.Modelo;



namespace ClinicaMedica.DAL
{
    public class PacienteDall
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection con = new MySqlConnection();

 

        public void IncluirPaciente(Paciente objPac)
        {
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "IncluirPaciente";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pnome", objPac.Nome);
                cmd.Parameters.AddWithValue("psexo", objPac.Sexo);
                cmd.Parameters.AddWithValue("pcpf", objPac.Cpf);
                cmd.Parameters.AddWithValue("prg", objPac.Rg);
                cmd.Parameters.AddWithValue("pdtaNascimento", objPac.DataNascimento);
                cmd.Parameters.AddWithValue("pendereco", objPac.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento", objPac.Complemento);
                cmd.Parameters.AddWithValue("ptelefoneFixo", objPac.TelFixo);
                cmd.Parameters.AddWithValue("pcelular", objPac.Celular);
                cmd.Parameters.AddWithValue("pemail", objPac.Email);
                cmd.Parameters.AddWithValue("pidConvenio", objPac.Convenio);
                

                con.Open();

                objPac.Prontuario = cmd.ExecuteScalar().ToString();
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                con.Close();
            }
         
        }

        public void AlterarPaciente(Paciente objPac)
        {
            try
            {

                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "AlterarPaciente";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pprontuario", objPac.Prontuario);
                cmd.Parameters.AddWithValue("pnome", objPac.Nome);
                cmd.Parameters.AddWithValue("psexo", objPac.Sexo);
                cmd.Parameters.AddWithValue("pcpf", objPac.Cpf);
                cmd.Parameters.AddWithValue("prg", objPac.Rg);
                cmd.Parameters.AddWithValue("pdtaNascimento", objPac.DataNascimento);
                cmd.Parameters.AddWithValue("pendereco", objPac.Endereco);
                cmd.Parameters.AddWithValue("pcomplemento", objPac.Complemento);
                cmd.Parameters.AddWithValue("ptelefoneFixo", objPac.TelFixo);
                cmd.Parameters.AddWithValue("pcelular", objPac.Celular);
                cmd.Parameters.AddWithValue("pemail", objPac.Email);
                cmd.Parameters.AddWithValue("pidConvenio", objPac.Convenio);
                

                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
            
        }


        public ListPaciente Listagem(string filtro)
        {
            ListPaciente objList = new ListPaciente();
            try
            {
                con.ConnectionString = Dados.strconexao;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = " ListarPaciente ";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("pfiltro", filtro);

                con.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                if(dr.HasRows == true )
                {
                   
                    while (dr.Read())
                    {
                        Paciente objPac = new Paciente();

                        objPac.Prontuario = dr["prontuario"].ToString();
                        objPac.Nome = dr["nome"].ToString();
                        objPac.Cpf = dr["Cpf"].ToString();
                        objPac.Rg = dr["Rg"].ToString();
                        objPac.Sexo =   dr["sexo"].ToString();
                        objPac.Celular = dr["celular"].ToString();
                        objPac.TelFixo = dr["TelefoneFixo"].ToString();
                        objPac.DataNascimento = Convert.ToDateTime( dr["dtaNascimento"]);
                        objPac.Endereco = dr["endereco"].ToString();
                        objPac.Complemento = dr["complemento"].ToString();
                        objPac.Email = dr["email"].ToString();
                      //  if (dr["idConvenio"].ToString() != null || dr["idConvenio"].ToString() == "")
                        //    objPac.Convenio = 0;
                       // else
                        objPac.Convenio =Convert.ToInt32(dr["idConvenio"].ToString());
                        
                        objList.Add(objPac);

                    }
                   
                }
                return objList;
            }
            catch(MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }
                


        }


    }
}
